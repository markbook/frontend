import path from 'path';

import { ConfigEnv, UserConfigExport } from 'vite';
import solid from 'vite-plugin-solid';

export default ({ command, mode }: ConfigEnv) =>
{
   const dev = mode === 'development';
   const config: UserConfigExport = {
      server: {
         proxy: {
            '/api': {
               target: 'http://localhost:5000',
               changeOrigin: true,
               rewrite: path => path.replace(/^\/api/, ''),
               timeout: 1000,
            }
         },
      },
      base: './',
      assetsInclude: ['*.gltf', /.gltf/],
      plugins: [
         solid({
            hot: dev,
            dev: dev,
            ssr: false,
         }),
      ],
      build: {
         polyfillDynamicImport: false,
         sourcemap: false,
         outDir: './distribution',
      },
      css: {
         modules: {
            generateScopedName: '[local]-[hash:base64:2]',
         },
      },
      resolve: {
         alias: [
            {
               find: '@',
               replacement: path.resolve(__dirname, './source'),
            },
         ],
      },
   };
   return config;
};
