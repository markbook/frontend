declare module '*.scss';
declare module '*?worker';
declare module '*.svg' {
	const content: string;
	export default content;
};
declare module '*.svg?raw' {
	const content: string;
	export default content;
};
declare module '*.png';
declare module '*.jpg';
declare module '*.webp';
declare module '*.gltf';
