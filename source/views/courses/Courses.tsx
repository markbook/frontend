import s from './Courses.module.scss';
import { createComputed, createEffect, createResource, For } from 'solid-js';
import { IUser } from '@/types/entities';
import { userClient } from '@/api/APIProvider';
export default function Courses() {
  const [user, load] = createResource<IUser | null>();
  createEffect(() => {
    load(() => userClient.getMe());
  });
  return (
    <div class={s.page}>
      {user()?.fullName}
      <div class={s.search}>
        <input type="text" />
      </div>
      <div class={s.courses}>
        {/* <For each={courses}>{(course) => <div class={s.card}>{course.title}</div>}</For> */}
      </div>
    </div>
  );
}
