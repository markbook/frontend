import '@/assets/styles/global.scss';
import '@/assets/styles/fonts.scss';
import { Router, useRoutes } from 'solid-app-router';
import { AppProvider } from '@/stores/AppStore';
import GlobalErrorWrapper from '@/wrappers/GlobalErrorWrapper';
import Error from './error';
import { routes } from '../router/Routes';
export default function App() {
   const Routes = useRoutes(routes);
   return (
      <Router>
         <AppProvider>
            <GlobalErrorWrapper />
            <Routes />
         </AppProvider>
      </Router>
   );
}
