import Traectory from '@/components/Traectory';
import { Course } from '@/components/Traectory/types';
import { createSignal, onMount } from 'solid-js';

import { sample4, sample, sample3, sample2, chemistry } from './test';
export default function Experiments() {
   let wolframContainer: HTMLDivElement;
   const [node, setNode] = createSignal(chemistry);
   const [rotation, setRotation] = createSignal(0);
   function randomize(maxDepth: number) {
      const node: Course = {
         title: `Предмет ${Math.round(Math.random() * 20)}`,
         closed: 0,
      };
      if (maxDepth == 0) {
         return node;
      }
      const childCount = Math.random() * 5;
      if (childCount > 0) {
         node.nodes = [];
         for (let i = 0; i < childCount; i++) {
            node.nodes.push(randomize(maxDepth - 1));
         }
      } else {
         node.title = `Тема ${Math.round(Math.random() * 20)}`;
      }
      return node;
   }
   onMount(() => {
      // const wolfram = WolframNotebookEmbedder.embed(
      //   'https://www.wolframcloud.com/obj/704f5524-2f61-4ae8-b64e-72256571a19f',
      //   wolframContainer
      // );
   });
   return (
      <div>
         {[sample, sample2, sample3, sample4, () => randomize(4)].map((x, i) => (
            <button
               onClick={() => {
                  const r = typeof x === 'function' ? x() : x;
                  console.log(r);
                  setNode(r);
               }}
            >
               {typeof x === 'function' ? 'random' : 'sample ' + i}
            </button>
         ))}
         <button onClick={() => setRotation((rotation() + Math.PI / 2) % (2 * Math.PI))}>rotate</button>
         <Traectory traectory={node()} zoomable height="900px" width="2000px" minZoom={200} maxZoom={600} zIndexZero rotate={rotation()} />

         <div ref={(r) => (wolframContainer = r)}></div>
         {/* <iframe src="https://desmos.com/" frameborder="0" width="400" height="900"></iframe> */}
      </div>
   );
}
