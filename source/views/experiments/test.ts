import { Course } from '@/components/Traectory/types';

export const sample: Course = {
   closed: 0,
   title: 'IT-специалист',
   color: '#E84033',
   nodes: [
      {
         closed: 0,
         title: 'Операционные системы',
         color: '#009B14',
         nodes: [
            {
               closed: 0,
               nodes: [
                  {
                     closed: 0,
                     title: 'Тема 4',
                     chapters: [
                        {
                           status: 2
                        },
                        {
                           status: 2
                        },
                        {
                           status: 1
                        },
                        {
                           status: 0
                        }
                     ],
                  },
                  {
                     closed: 0,
                     title: 'Тема 5',
                     chapters: [
                        {
                           status: 2
                        },
                        {
                           status: 2
                        },
                        {
                           status: 1
                        },
                        {
                           status: 0
                        }
                     ],
                  },
                  {
                     closed: 0,
                     title: 'Предмет 228',
                     color: '#457e94',
                     nodes: [
                        {
                           closed: 0,
                           title: 'Тема 4',
                           chapters: [
                              {
                                 status: 2
                              },
                              {
                                 status: 2
                              },
                              {
                                 status: 1
                              },
                           ],
                        },
                        {
                           closed: 0,
                           title: 'Тема 5',
                           chapters: [
                              {
                                 status: 2
                              },
                              {
                                 status: 2
                              },
                              {
                                 status: 1
                              },
                              {
                                 status: 0
                              }
                           ],
                        },
                     ],
                  },
               ],
               title: 'Алгоритмы и анализ сложности',
               color: '#F2B826',
            },
            {
               closed: 0,
               title: 'Мьютексы',
            },
         ],
      },
      {
         closed: 0,
         title: 'Теория графов',
         color: '#5192CE',
         nodes: [
            {
               title: 'Предмет 0',
               color: '#8334FE',
               closed: 0,
               nodes: [
                  {
                     title: 'Дискретная математика',
                     color: '#83348E',
                     closed: 0,
                     nodes: [
                        {
                           closed: 0,
                           title: 'Тема 3',
                           chapters: [],
                        },
                     ],
                  },
                  {
                     closed: 0,
                     nodes: [
                        {
                           closed: 0,
                           title: 'Тема 2',
                        },
                     ],
                     title: 'Предмет 6',
                  },
                  {
                     closed: 0,
                     title: 'Тема 1',
                  },
                  {
                     title: 'Тема 7',
                     color: '#83348E',
                     closed: 0,
                     nodes: [
                        {
                           closed: 0,
                           title: 'Тема 4',
                        },
                     ],
                  },
               ],
            },
         ],
      },
      {
         closed: 0,
         title: 'Рандомная тема',
      },
      {
         closed: 0,
         title: 'Предмет 2',
         color: '#6c539c',
         nodes: [
            {
               closed: 0,
               title: 'Предмет 3',
               nodes: [
                  {
                     closed: 0,
                     title: 'Предмет 4',
                     nodes: [],
                  },
               ],
            },
            {
               closed: 0,
               title: 'Предмет 5',
               nodes: [],
            },
         ],
      },
      {
         closed: 0,
         title: 'Системная инженерия',
      },
      {
         closed: 0,
         title: 'Предмет 1',
         nodes: [
            {
               closed: 0,
               title: 'Системная инженерия 2',
            },
            {
               closed: 0,
               title: 'Предмет 5',
               nodes: [],
               color: '#457e94',
            },
         ],
      },
   ],
};
export const sample2 = {
   title: 'Предмет 15',
   closed: 0,
   nodes: [
      {
         title: 'Предмет 13',
         closed: 0,
         nodes: [
            {
               title: 'Предмет 16',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 17',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 17',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 2',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 12',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 15',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 3',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 5',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 20',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 11',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 18',
                     closed: 0,
                  },
               ],
            },
         ],
      },
      {
         title: 'Предмет 10',
         closed: 0,
         nodes: [
            {
               title: 'Предмет 19',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 2',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 12',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 16',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 6',
                     closed: 0,
                  },
               ],
            },
         ],
      },
      {
         title: 'Предмет 19',
         closed: 0,
         nodes: [
            {
               title: 'Предмет 14',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 12',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 11',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 12',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 12',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 19',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 3',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 8',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 14',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 7',
                     closed: 0,
                  },
               ],
            },
         ],
      },
      {
         title: 'Предмет 16',
         closed: 0,
         nodes: [
            {
               title: 'Предмет 18',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 10',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 20',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 17',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 15',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 18',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 0',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 19',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 18',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 10',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 5',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 16',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 15',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 11',
                     closed: 0,
                  },
               ],
            },
         ],
      },
      {
         title: 'Предмет 14',
         closed: 0,
         nodes: [
            {
               title: 'Предмет 7',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 19',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 10',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 2',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 7',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 0',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 9',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 19',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 2',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 16',
                     closed: 0,
                  },
               ],
            },
         ],
      },
   ],
};

export const sample3 = {
   title: 'Предмет 8',
   closed: 0,
   nodes: [
      {
         title: 'Предмет 19',
         closed: 0,
         nodes: [
            {
               title: 'Предмет 3',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 20',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 16',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 6',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 14',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 6',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 8',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 15',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 6',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 15',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 8',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 12',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 19',
                     closed: 0,
                  },
               ],
            },
         ],
      },
      {
         title: 'Предмет 8',
         closed: 0,
         nodes: [
            {
               title: 'Предмет 11',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 19',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 18',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 0',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 19',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 9',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 16',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 7',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 5',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 7',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 14',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 17',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 13',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 1',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 3',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 13',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 17',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 2',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 8',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 14',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 20',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 20',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 15',
                     closed: 0,
                  },
               ],
            },
         ],
      },
      {
         title: 'Предмет 6',
         closed: 0,
         nodes: [
            {
               title: 'Предмет 15',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 11',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 15',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 15',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 6',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 10',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 15',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 17',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 6',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 5',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 16',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 6',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 4',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 10',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 14',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 13',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 11',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 8',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 1',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 10',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 9',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 1',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 3',
                     closed: 0,
                  },
               ],
            },
         ],
      },
      {
         title: 'Предмет 12',
         closed: 0,
         nodes: [
            {
               title: 'Предмет 11',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 14',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 17',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 18',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 12',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 18',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 11',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 3',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 0',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 2',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 9',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 19',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 8',
                     closed: 0,
                  },
               ],
            },
         ],
      },
      {
         title: 'Предмет 7',
         closed: 0,
         nodes: [
            {
               title: 'Предмет 18',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 9',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 5',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 19',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 13',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 15',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 16',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 2',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 2',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 12',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 0',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 1',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 14',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 10',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 14',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 3',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 12',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 0',
                     closed: 0,
                  },
               ],
            },
         ],
      },
   ],
};
export const sample4 = {
   title: 'Предмет 17',
   closed: 0,
   nodes: [
      {
         title: 'Предмет 9',
         closed: 0,
         nodes: [
            {
               title: 'Предмет 11',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 14',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 13',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 13',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 15',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 11',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 1',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 5',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 19',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 19',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 1',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 15',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 2',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 19',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 5',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 16',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 11',
                     closed: 0,
                  },
               ],
            },
         ],
      },
      {
         title: 'Предмет 11',
         closed: 0,
         nodes: [
            {
               title: 'Предмет 5',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 10',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 16',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 13',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 18',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 4',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 2',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 13',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 5',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 10',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 3',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 14',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 1',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 5',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 7',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 16',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 11',
                     closed: 0,
                  },
               ],
            },
         ],
      },
      {
         title: 'Предмет 17',
         closed: 0,
         nodes: [
            {
               title: 'Предмет 14',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 7',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 10',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 4',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 19',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 9',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 6',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 17',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 0',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 6',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 2',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 15',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 1',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 2',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 1',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 13',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 1',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 11',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 20',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 11',
                     closed: 0,
                  },
               ],
            },
         ],
      },
      {
         title: 'Предмет 19',
         closed: 0,
         nodes: [
            {
               title: 'Предмет 12',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 1',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 20',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 6',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 9',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 16',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 1',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 12',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 6',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 0',
                     closed: 0,
                  },
               ],
            },
         ],
      },
      {
         title: 'Предмет 10',
         closed: 0,
         nodes: [
            {
               title: 'Предмет 6',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 3',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 15',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 3',
                     closed: 0,
                  },
               ],
            },
            {
               title: 'Предмет 0',
               closed: 0,
               nodes: [
                  {
                     title: 'Предмет 16',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 10',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 18',
                     closed: 0,
                  },
                  {
                     title: 'Предмет 2',
                     closed: 0,
                  },
               ],
            },
         ],
      },
   ],
};
export const chemistry: Course = {
   title: 'Аналитическая химия',
   nodes: [
      {
         title: 'История и философия науки',
         nodes: [
            {
               title: 'Позитивизм'
            },
            {
               title: 'Постпозитивизм'
            },
            {
               title: 'Научные войны'
            },
         ],
      },
      {
         title: 'Научные коммуникации',
         nodes: [
            {
               title: 'Soft-skills'
            }
         ]
      },
      {
         title: 'Актуальные задачи химии и химической технологии',
         nodes: [
            {
               title: 'Методика научных исследований'
            },
            {
               title: 'Современные технологии в химической промышленности'
            },
            {
               title: 'Неорганическая и органическая химия в экстремальных условиях'
            }
         ]
      }
   ],
};

export const biophysics: Course = {
   title: 'Медицинская биофизика',
   nodes: [
      {
         title: 'Мировоззренчиские основы профессиональной деятельности',
         nodes: [
            {
               title: 'История медицины'
            },
            {
               title: 'Клятва Гиппократа'
            }
         ]
      },
      {
         title: 'Экономика',
         nodes: [
            {
               title: 'История экономики'
            },
            {
               title: 'Секторы экономики'
            },
            {
               title: 'Формы экономики'
            }
         ]
      },
      {
         title: 'Безопасность жизнедеятельности'
      },
      {
         title: 'Неврология и психиатрия'
      },
      {
         title: 'Инфекционные болезни',
         nodes: [
            {
               title: 'Болезни, передавающиеся половым путем'
            },
            {
               title: 'Вирусные инфекции'
            },
            {
               title: 'Бактериальные инфекции'
            },
         ]
      }
   ]
};