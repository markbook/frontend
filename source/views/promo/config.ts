export const BALLS_END_DELAY = 1000;
export const EXTRA_CALC_LENGTH = 300;
export const LINE_MOVE_DURATION = 0.07;
export const TEXT_DURATION = 0.05;
export const ZOOM_DURATION = 0.04;