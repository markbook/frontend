import TrDemo from './assets/traectory1.svg?raw';
import ComputerImage from './assets/first_pc.jpg';
import TableImage from './assets/table.jpg';
import UniversityImage from './assets/university.jpg';
import GraphicImage from './assets/graf.jpg';
import LampImage from './assets/lampochka.png';
import LeftLogo from './assets/LeftLogo.svg';
import Polygon1 from './assets/Polygon1.svg';
import Polygon2 from './assets/Polygon2.svg';
import Polygon3 from './assets/Polygon3.svg';
import s from './Promo.module.scss';

import useMediaQuery from '@solid-primitives/media';
import { useWhiteBall, useColoredBalls, useShowreel, useIntroTraectory } from './interactives';
import { EXTRA_CALC_LENGTH } from './config';
import { progress } from './helpers';
import { TrackColored } from './TrackColored';
import { TrackOutlined } from './TrackOutlined';
import DevInfo from '@/components/DevInfo';

export default function Promo ()
{
    let mainRef: HTMLElement;
    let ballRef: HTMLElement;
    let aiBallRef: HTMLElement;
    let traectoryRef: HTMLElement;

    const isMobile = useMediaQuery('screen and (max-width: 600px)', false, true);
    const showIntroTraectory = useIntroTraectory();
    const [colorBalls, , colorBallsAnimator] = useColoredBalls();
    const [aiBallShown, , aiBallAnimator] = useWhiteBall();
    const [showreel, setShowreel, showreelAnimator, mobileShowreelAnimator] = useShowreel();

    const mailto = 'mailto:support@маркбук.рф'
    function onWindowScroll ()
    {
        const clrBallsProg = ballRef.offsetTop / (ballRef.parentElement!.offsetHeight - ballRef.offsetHeight);
        colorBallsAnimator.frame(clrBallsProg);

        const aiBallBound = aiBallRef.getBoundingClientRect();
        const half = aiBallBound.height / 2;
        const whiteBallProgress = progress(-half, half, window.scrollY - aiBallBound.top - screen.availHeight / 2.5);
        aiBallAnimator.frame(whiteBallProgress);

        const trDelta = mainRef.scrollTop - (traectoryRef!.offsetParent! as HTMLElement).offsetTop;
        if (trDelta < -EXTRA_CALC_LENGTH)
        {
            showreelAnimator.reset();
            return;
        }

        const trProgress = traectoryRef.offsetTop / (traectoryRef.parentElement!.offsetHeight - traectoryRef.offsetHeight);
        if (isMobile())
        {
            mobileShowreelAnimator.frame(trProgress);
        } else
        {
            showreelAnimator.frame(trProgress);
        }
    }

    return (
        <main class={s.landing} ref={(r) => (mainRef = r)} onScroll={onWindowScroll}>
            {/* <DevInfo data={Object.assign({}, colorBalls, aiBallShown, showreel)} /> */}
            <img class={s.polygon} src={Polygon1} />
            <img class={s.polygon} src={Polygon2} />
            <img class={s.polygon} src={Polygon3} />
            <section class={s.promo}>
                <div class={s.trdemo}>
                    <div class={s.trimg} innerHTML={TrDemo} />
                    <div classList={{ [s.shadow]: true, [s.filled]: showIntroTraectory() }} innerHTML={TrDemo} />
                </div>
                <header class={s.header}>
                    {/* <div class={s.logo} /> */}
                    <img src={LeftLogo} class={s.logo} />
                    <nav class={s.nav}>
                        <a href="#features">Преимущества</a>
                        <a href="#balls">Как это работает?</a>
                        <a href="#traectory">Траектория</a>
                        <a href={mailto}>Написать нам</a>
                        <a class={s.contact} href="mailto:support@маркбук.рф">
                            <div>Написать нам</div>
                        </a>
                    </nav>
                </header>
                <div class={s.first}>
                    <div class={s.border}>Уникальный путь к знаниям</div>
                    <div class={s.description}>
                        Индивидуальная образовательная траектория до любой цели, будь то поступление в ВУЗ, смена профессии, карьерный рост, погружение в
                        науку при взаимодействии с учебными материалами, преподавателями и организациями.
                    </div>
                    <div class={s.chipsBlock}>
                        <span>Маркбук подходит вам, если вы:</span>
                        <div class={s.chips}>
                            {[['Школьник', s.pupil], ['Родитель', s.parent], ['Студент', s.student]].map(([t, c]) => (
                                <div classList={{ [s.chip]: true, [c]: true }}>{t}</div>
                            ))}
                        </div>
                        <div class={s.chips}>
                            {[['Репетитор', s.spTeacher], ['Организатор курсов', s.contentMaker], ['Преподаватель', s.teacher]].map(([t, c]) => (
                                <div classList={{ [s.chip]: true, [c]: true }}>{t}</div>
                            ))}
                        </div>
                    </div>
                </div>
            </section>
            <section id="features" class={s.featuresSection}>
                <div class={s.features}>
                    <div class={s.title}>Мы совершенствуем процесс и качество образования:</div>

                    {[
                        [ComputerImage, 'Совмещение оффлайн и онлайн образования', 'Очные, заочные занятия в Вашей траектории на выбор'],
                        [TableImage, 'Ученик и преподаватель взаимодействиют лично', 'Преподаватель с согласия ученика увидит его интересы, стремления и цели и подберет оптимальный подход'],
                        [GraphicImage, 'Уменьшение нагрузки на ученика и преподавателя', 'Продвинутая система учета всех метрик, успеваемости сгенерирует все необходимые отчеты'],
                        [UniversityImage, 'Качественные учебные материалы от лучших вузов, школ и преподавателей', 'Маркбук анализирует успехи большого количества учеников - поэтому рекомендуются только те курсы, которые действительно помогли добиться цели'],
                        [LampImage, 'Совместимость со стандартной системой образования', 'Траектория строится включая в себя многие курсы из Вашего текущего учебного заведения или аналогичные'],
                    ].map(([img, title, description]) => (
                        <div class={s.feature}>
                            <div
                                class={s.picture}
                            ><img src={img} /></div>
                            <div class={s.textBlock}>
                                <div class={s.text}>{title}</div>
                                <div class={s.description}>{description}</div>
                            </div>
                        </div>
                    ))}
                </div>
            </section>
            <section id="balls">
                <div class={s.balls}>
                    <div class={s.stickyContainer}>
                        <div class={s.stickyContent} ref={(r) => (ballRef = r)}>
                            <div class={s.title}>Как это работает?</div>
                            {Object.entries(colorBalls).map(([key, { title, x, y, fade, size }]) => (
                                <div
                                    classList={{
                                        [s.ball]: true,
                                        [s[key as 'orange' | 'pink' | 'blue']]: true,
                                    }}
                                    style={{
                                        transform: `scale(${size}) translate(${x}%, ${y}%)`,
                                        opacity: fade,
                                    }}
                                >
                                    {title}
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
                <div ref={(r) => (aiBallRef = r)}>
                    <div class={s.symbol}>+</div>
                    <div
                        classList={{
                            [s.ball]: true,
                            [s.white]: true,
                            [aiBallShown()]: true,
                        }}
                    >
                        Искусственный интеллект
                    </div>
                    <div class={s.symbol}>=</div>
                </div>
            </section>
            <section class={s.reel} id="traectory">
                <div class={s.stickyContainer}>
                    <div class={s.stickyContent} ref={(r) => (traectoryRef = r)}>
                        <div
                            class={s.scene}
                            classList={{
                                [s.scene]: true,
                                [s.mobile]: isMobile(),
                                [showreel.scene.class]: true,
                                [showreel.scene.class1]: true,
                            }}
                            style={{
                                transform: `translate(${showreel.scene.transform.translate.x}, ${showreel.scene.transform.translate.y}) scale(${showreel.scene.transform.scale})`,
                            }}
                        >
                            <div
                                class={s.title}
                                style={{
                                    opacity: showreel.titleOpacity,
                                }}
                            >
                                Индивидуальная траектория
                            </div>
                            <div class={s.box}>
                                <div
                                    classList={{
                                        [s.calendar]: true,
                                        [showreel.calendar.class]: true,
                                    }}
                                    style={{
                                        opacity: showreel.calendar.opacity,
                                    }}
                                />
                                <div
                                    classList={{ [s.line]: true, [showreel.timeline.class]: true }}
                                    style={{
                                        left: showreel.timeline.left,
                                    }}
                                />
                                <TrackColored
                                    classList={{
                                        [s.track]: true,
                                        [s.colored]: true,
                                        [showreel.color.class]: true,
                                    }}
                                />
                                <TrackOutlined
                                    classList={{
                                        [s.track]: true,
                                        [s.outlined]: true,
                                        [showreel.outline.class]: true,
                                    }}
                                    style={{
                                        'clip-path': `polygon(${Object.values(showreel.outline.clip).reduce(
                                            (prev, curr, i, arr) => (prev !== '' ? prev + ' ' : prev) + curr + (i !== arr.length - 1 && i % 2 === 1 ? ',' : '')
                                        )})`,
                                    }}
                                />
                                <div
                                    classList={{
                                        [s.modal]: true,
                                        [s.course]: true,
                                        [showreel.modals.course]: true,
                                    }}
                                >
                                    <div class={s.modalInner}>
                                        <div class={s.modalTitle}>Курс</div>
                                        <div class={s.description}>С прохождением заданий в курсе линия будет заполняться и вы увидете прогресс</div>
                                    </div>
                                </div>
                                <div
                                    classList={{
                                        [s.modal]: true,
                                        [s.chapter]: true,
                                        [showreel.modals.courseChapter]: true,
                                    }}
                                >
                                    <div class={s.modalInner}>
                                        <div class={s.modalTitle}>Тема</div>
                                        <div class={s.description}>В каждом курсе есть темы, отмеченные кружами. Каждая тема состоит из разных заданий</div>
                                    </div>
                                </div>
                                <div
                                    classList={{
                                        [s.modal]: true,
                                        [s.arrowDep]: true,
                                        [showreel.modals.arrowDep]: true,
                                    }}
                                >
                                    <div class={s.modalInner}>
                                        <div class={s.modalTitle}>Связь</div>
                                        <div class={s.description}>Ранее полученные знания требуется для успешного усвоения курса</div>
                                    </div>
                                </div>
                                <div
                                    classList={{
                                        [s.modal]: true,
                                        [s.dep]: true,
                                        [showreel.modals.dep]: true,
                                    }}
                                >
                                    <div class={s.modalInner}>
                                        <div class={s.modalTitle}>Иерархия</div>
                                        <div class={s.description}>Курс открывает возможность использовать полученные знания</div>
                                    </div>
                                </div>
                                <div
                                    classList={{
                                        [s.modal]: true,
                                        [s.main]: true,
                                        [showreel.modals.main]: true,
                                    }}
                                >
                                    <div class={s.modalInner}>
                                        <div class={s.modalTitle}>Контроль</div>
                                        <div class={s.description}>Тестирование по завершенным курсам или практика</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <footer>
                <a href={mailto}>
                    Написать нам 🚀😌
                </a>
            </footer>
        </main>
    );
}
