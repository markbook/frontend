import { createEffect, createSignal } from 'solid-js';
import { createStore } from 'solid-js/store';
import useMediaQuery from '@solid-primitives/media';
import { Animation } from './animator';
import { LINE_MOVE_DURATION, TEXT_DURATION, ZOOM_DURATION } from './config';
import s from './Promo.module.scss';

export function useIntroTraectory() {
   const [fakeTraectoryReleased, setFakeTraectoryReleased] = createSignal(false);

   createEffect(() => {
      setTimeout(() => {
         setFakeTraectoryReleased(true);
      }, 1000);
   });
   return fakeTraectoryReleased;
}

export function useColoredBalls() {
   const [state, setState] = createStore({
      orange: {
         title: 'Цели',
         x: 0,
         y: 0,
         z: 0,
         fade: 0,
         size: 0.7,
      },
      pink: {
         title: 'Интересы',
         x: 0,
         y: 0,
         z: 0,
         fade: 1,
         size: 0.7,
      },
      blue: {
         title: 'Стремления',
         x: 0,
         y: 300,
         z: 0,
         fade: 0,
         size: 0,
      },
   });
   const ballsAnimator = new Animation()
      .play(
         (p) => {
            setState('orange', 'fade', p);
            setState('orange', 'size', 0.7 + p * 0.3);
         },
         0.6,
         0.6
      )
      .play(
         (p) => {
            setState('pink', 'fade', p);
            setState('pink', 'size', 0.7 + p * 0.4);
            setState('pink', 'x', p * 130);
            setState('pink', 'y', p * -55);
         },
         0.7,
         0.6
      )
      .play(
         (p) => {
            setState('blue', 'fade', p);
            setState('blue', 'size', 0.3 + p * 0.9);
            setState('blue', 'x', p * -110);
            setState('blue', 'y', p * -45);
         },
         0.9,
         0
      )
      .compile();
   return [state, setState, ballsAnimator] as const;
}
export function useWhiteBall() {
   const [cl, setClass] = createSignal(s.hidden);
   const whiteBallAnimator = new Animation()
      .once(
         () => setClass(s.hidden),
         () => setClass(s.shown)
      )
      .compile();
   return [cl, setClass, whiteBallAnimator] as const;
}

export function useShowreel() {
   const isMobile = useMediaQuery('screen and (max-width: 600px)');

   const [state, setState] = createStore({
      fakeTraectoryReleased: false,
      titleOpacity: 1,
      scene: {
         class: '',
         class1: '',
         transform: {
            translate: {
               x: '0%',
               y: '0%',
            },
            rotate: '0',
            scale: 1,
         },
      },
      calendar: {
         class: '',
         opacity: 0,
      },
      timeline: {
         class: '',
         left: '0%',
      },
      color: {
         class: '',
         // opacity: 1,
      },
      outline: {
         class: '',
         // opacity: 0,
         clip: {
            x1: '0%',
            y1: '0%',
            x2: '100%',
            y2: '0%',
            x3: '100%',
            y3: '100%',
            x4: '0%',
            y4: '100%',
         },
      },
      modals: {
         course: s.hideModal,
         courseChapter: s.hideModal,
         arrowDep: s.hideModal,
         dep: s.hideModal,
         main: s.hideModal,
      },
   });
   const traectoryAnimator = new Animation()
      .once(
         () => setState('color', 'class', s.invisible),
         () => setState('color', 'class', s.visible)
      )
      .once(
         () => setState('timeline', 'class', s.invisible),
         () => setState('timeline', 'class', s.visible)
      )
      .wait(0.02)
      .play((p) => setState('calendar', 'opacity', p), 0.01)
      .wait(0.01)
      .once(
         () => setState('outline', 'class', s.invisible),
         () => setState('outline', 'class', s.visible)
      )
      .wait(0.01)
      .once(
         () => setState('color', 'class', s.invisible),
         () => setState('color', 'class', s.visible)
      )
      .once(
         () => setState('timeline', 'class', s.invisible),
         () => setState('timeline', 'class', s.visible)
      )
      .wait(0.01)
      /**КУРС */
      .play((p) => {
         const n = p * 35 + '%';
         setState('outline', 'clip', 'x1', n);
         setState('outline', 'clip', 'x4', n);
         const l = p * 35 + '%';
         setState('timeline', 'left', l);
      }, LINE_MOVE_DURATION)
      .play(
         (p) => {
            // Зумим
            const x = p * 30 + '%';
            const y = p * 30 + '%';
            const z = 1 + p * 0.5;
            setState('scene', 'transform', 'translate', 'x', x);
            setState('scene', 'transform', 'translate', 'y', y);
            setState('scene', 'transform', 'scale', z);
            setState('titleOpacity', 1 - p);
         },
         ZOOM_DURATION,
         0
      )
      .once(
         // Показываем текст
         () => {
            setState('modals', 'course', s.hideModal);
            setState('scene', 'class', '');
         },
         () => {
            setState('modals', 'course', s.showModal);
            setState('scene', 'class', s.focusCourse);
         }
      )
      .wait(TEXT_DURATION)
      .once(
         // Показываем текст
         () => {
            setState('modals', 'course', s.showModal);
            setState('scene', 'class', s.focusCourse);
         },
         () => {
            setState('modals', 'course', s.hideModal);
            setState('scene', 'class', '');
         }
      )
      .wait(TEXT_DURATION / 2)
      /**КУРС */
      /**ТЕМА */
      .play(
         (p) => {
            // Зумим
            const y = 30 - p * 50 + '%';
            setState('scene', 'transform', 'translate', 'y', y);
         },
         ZOOM_DURATION,
         0
      )
      .once(
         // Показываем текст
         () => {
            setState('modals', 'courseChapter', s.hideModal);
            setState('scene', 'class', '');
         },
         () => {
            setState('modals', 'courseChapter', s.showModal);
            setState('scene', 'class', s.focusChapter);
         }
      )
      .wait(TEXT_DURATION)
      .once(
         // Показываем текст
         () => {
            setState('modals', 'courseChapter', s.showModal);
            setState('scene', 'class', s.focusChapter);
         },
         () => {
            setState('modals', 'courseChapter', s.hideModal);
            setState('scene', 'class', '');
         }
      )
      .wait(TEXT_DURATION / 2)
      .play(
         (p) => {
            // Зумим
            const x = 30 - p * 30 + '%';
            const y = -20 + p * 20 + '%';
            const z = 1.5 - p * 0.5;
            setState('scene', 'transform', 'translate', 'x', x);
            setState('scene', 'transform', 'translate', 'y', y);
            setState('scene', 'transform', 'scale', z);
         },
         ZOOM_DURATION,
         0
      )
      .play((p) => {
         const s = 35;
         const n = s + p * (54 - s) + '%';
         setState('outline', 'clip', 'x1', n);
         setState('outline', 'clip', 'x4', n);
         const l = s + p * (54 - s) + '%';
         setState('timeline', 'left', l);
      }, LINE_MOVE_DURATION)
      .play(
         (p) => {
            // Зумим
            const x = p * 5 + '%';
            const z = 1 + p * 0.5;
            setState('scene', 'transform', 'translate', 'x', x);
            setState('scene', 'transform', 'scale', z);
         },
         ZOOM_DURATION,
         0
      )
      /**ТЕМА */
      .once(
         // Показываем текст
         () => {
            setState('modals', 'arrowDep', s.hideModal);
            setState('scene', 'class', '');
         },
         () => {
            setState('modals', 'arrowDep', s.showModal);
            setState('scene', 'class', s.focusArrowDep);
         }
      )
      .wait(TEXT_DURATION)
      .once(
         // Показываем текст
         () => {
            setState('modals', 'arrowDep', s.showModal);
            setState('scene', 'class', s.focusArrowDep);
         },
         () => {
            setState('modals', 'arrowDep', s.hideModal);
            setState('scene', 'class', '');
         }
      )
      .wait(TEXT_DURATION / 2)
      .play((p) => {
         const s = 54;
         const n = s + p * (69 - s) + '%';
         setState('outline', 'clip', 'x1', n);
         setState('outline', 'clip', 'x4', n);
         const l = s + p * (69 - s) + '%';
         setState('timeline', 'left', l);
      }, LINE_MOVE_DURATION)
      .play((p) => {
         const s = 5;
         // Едем
         const x = s - p * 10 + '%';
         setState('scene', 'transform', 'translate', 'x', x);
      }, ZOOM_DURATION)
      .once(
         // Показываем текст
         () => {
            setState('modals', 'dep', s.hideModal);
            setState('scene', 'class', '');
         },
         () => {
            setState('modals', 'dep', s.showModal);
            setState('scene', 'class', s.focusDep);
         }
      )
      .wait(TEXT_DURATION)
      .once(
         // Показываем текст
         () => {
            setState('modals', 'dep', s.showModal);
            setState('scene', 'class', s.focusDep);
         },
         () => {
            setState('modals', 'dep', s.hideModal);
            setState('scene', 'class', '');
         }
      )
      .wait(TEXT_DURATION / 2)
      .play(
         (p) => {
            const s = -5;
            // Едем
            const x = s - p * 45 + '%';
            setState('scene', 'transform', 'translate', 'x', x);
         },
         ZOOM_DURATION,
         ZOOM_DURATION
      )
      .play((p) => {
         const s = 69;
         const n = s + p * (100 - s) + '%';
         setState('outline', 'clip', 'x1', n);
         setState('outline', 'clip', 'x4', n);
         const l = s + p * (100 - s) + '%';
         setState('timeline', 'left', l);
      }, LINE_MOVE_DURATION)
      .once(
         // Показываем текст
         () => {
            setState('modals', 'main', s.hideModal);
            setState('scene', 'class', '');
         },
         () => {
            setState('modals', 'main', s.showModal);
            setState('scene', 'class', s.focusMain);
         }
      )
      .wait(TEXT_DURATION)
      .once(
         // Показываем текст
         () => {
            setState('modals', 'main', s.showModal);
            setState('scene', 'class', s.focusMain);
         },
         () => {
            setState('modals', 'main', s.hideModal);
            setState('scene', 'class', '');
         }
      )
      .wait(TEXT_DURATION / 2)
      .play(
         (p) => {
            // Зумим
            const x = -50 + p * 50 + '%';
            const z = 1.5 - p * 0.5;
            setState('scene', 'transform', 'translate', 'x', x);
            setState('scene', 'transform', 'scale', z);
         },
         ZOOM_DURATION,
         0
      )
      .once(
         // Показываем текст
         () =>
            Object.keys(state.modals).map((key) => setState('modals', key as 'course' | 'courseChapter' | 'arrowDep' | 'dep' | 'main', s.hideModal)),
         () =>
            Object.keys(state.modals).map((key) => setState('modals', key as 'course' | 'courseChapter' | 'arrowDep' | 'dep' | 'main', s.showModal))
      )
      .once(
         () => setState('titleOpacity', 0),
         () => setState('titleOpacity', 1)
      )
      .compile();

   /** MOBILE */
   const mobileTraectoryAnimator = new Animation()
      .once(
         () => setState('color', 'class', s.invisible),
         () => setState('color', 'class', s.visible)
      )
      .once(
         () => setState('timeline', 'class', s.invisible),
         () => setState('timeline', 'class', s.visible)
      )
      .wait(0.02)
      .play((p) => setState('calendar', 'opacity', p), 0.01)
      .wait(0.01)
      .once(
         () => setState('outline', 'class', s.invisible),
         () => setState('outline', 'class', s.visible)
      )
      .wait(0.01)
      .once(
         () => setState('color', 'class', s.invisible),
         () => setState('color', 'class', s.visible)
      )
      .once(
         () => setState('timeline', 'class', s.invisible),
         () => setState('timeline', 'class', s.visible)
      )
      .wait(0.01)
      .once(
         () => {
            setState('scene', 'class1', '');
         },
         () => {
            setState('scene', 'class1', s.zoomed);
         }
      )
      .wait(0.02)
      /**КУРС */
      .play((p) => {
         const n = p * 35 + '%';
         setState('outline', 'clip', 'x1', n);
         setState('outline', 'clip', 'x4', n);
         const l = p * 35 + '%';
         setState('timeline', 'left', l);
         const x = -p * 15 + '%';
         setState('scene', 'transform', 'translate', 'x', x);
      }, LINE_MOVE_DURATION)
      .once(
         // Показываем текст
         () => {
            setState('modals', 'course', s.hideModal);
            setState('scene', 'class', '');
         },
         () => {
            setState('modals', 'course', s.showModal);
            setState('scene', 'class', s.focusCourse);
         }
      )
      .wait(TEXT_DURATION)
      .once(
         // Показываем текст
         () => {
            setState('modals', 'course', s.showModal);
            setState('scene', 'class', s.focusCourse);
         },
         () => {
            setState('modals', 'course', s.hideModal);
            setState('scene', 'class', '');
         }
      )
      .wait(TEXT_DURATION / 2)
      /**КУРС */
      /**ТЕМА */
      .once(
         // Показываем текст
         () => {
            setState('modals', 'courseChapter', s.hideModal);
            setState('scene', 'class', '');
         },
         () => {
            setState('modals', 'courseChapter', s.showModal);
            setState('scene', 'class', s.focusChapter);
         }
      )
      .wait(TEXT_DURATION)
      .once(
         // Показываем текст
         () => {
            setState('modals', 'courseChapter', s.showModal);
            setState('scene', 'class', s.focusChapter);
         },
         () => {
            setState('modals', 'courseChapter', s.hideModal);
            setState('scene', 'class', '');
         }
      )
      .wait(TEXT_DURATION / 2)
      .play((p) => {
         const s = 35;
         const n = s + p * (54 - s) + '%';
         setState('outline', 'clip', 'x1', n);
         setState('outline', 'clip', 'x4', n);
         const l = s + p * (54 - s) + '%';
         setState('timeline', 'left', l);
         const x = -15 - p * 15 + '%';
         setState('scene', 'transform', 'translate', 'x', x);
      }, LINE_MOVE_DURATION)
      /**ТЕМА */
      .once(
         // Показываем текст
         () => {
            setState('modals', 'arrowDep', s.hideModal);
            setState('scene', 'class', '');
         },
         () => {
            setState('modals', 'arrowDep', s.showModal);
            setState('scene', 'class', s.focusArrowDep);
         }
      )
      .wait(TEXT_DURATION)
      .once(
         // Показываем текст
         () => {
            setState('modals', 'arrowDep', s.showModal);
            setState('scene', 'class', s.focusArrowDep);
         },
         () => {
            setState('modals', 'arrowDep', s.hideModal);
            setState('scene', 'class', '');
         }
      )
      .wait(TEXT_DURATION / 2)
      .play((p) => {
         const s = 54;
         const n = s + p * (69 - s) + '%';
         setState('outline', 'clip', 'x1', n);
         setState('outline', 'clip', 'x4', n);
         const l = s + p * (69 - s) + '%';
         setState('timeline', 'left', l);
         const x = -30 - p * 15 + '%';
         setState('scene', 'transform', 'translate', 'x', x);
      }, LINE_MOVE_DURATION)
      .once(
         // Показываем текст
         () => {
            setState('modals', 'dep', s.hideModal);
            setState('scene', 'class', '');
         },
         () => {
            setState('modals', 'dep', s.showModal);
            setState('scene', 'class', s.focusDep);
         }
      )
      .wait(TEXT_DURATION)
      .once(
         // Показываем текст
         () => {
            setState('modals', 'dep', s.showModal);
            setState('scene', 'class', s.focusDep);
         },
         () => {
            setState('modals', 'dep', s.hideModal);
            setState('scene', 'class', '');
         }
      )
      .wait(TEXT_DURATION / 2)
      .play((p) => {
         const s = 69;
         const n = s + p * (100 - s) + '%';
         setState('outline', 'clip', 'x1', n);
         setState('outline', 'clip', 'x4', n);
         const l = s + p * (100 - s) + '%';
         setState('timeline', 'left', l);
         const x = -45 - p * 30 + '%';
         setState('scene', 'transform', 'translate', 'x', x);
      }, LINE_MOVE_DURATION)
      .once(
         // Показываем текст
         () => {
            setState('modals', 'main', s.hideModal);
            setState('scene', 'class', '');
         },
         () => {
            setState('modals', 'main', s.showModal);
            setState('scene', 'class', s.focusMain);
         }
      )
      .wait(TEXT_DURATION)
      .once(
         // Показываем текст
         () => {
            setState('modals', 'main', s.showModal);
            setState('scene', 'class', s.focusMain);
         },
         () => {
            setState('modals', 'main', s.hideModal);
            setState('scene', 'class', '');
         }
      )
      .once(
         () => {
            setState('scene', 'class1', s.zoomed);
            setState('scene', 'transform', 'translate', 'x', '-75%');
            setState('titleOpacity', 0);
         },
         () => {
            setState('scene', 'class1', '');
            setState('scene', 'transform', 'translate', 'x', '0%');
            setState('titleOpacity', 1);
         }
      )
      .compile();

   return [state, setState, traectoryAnimator, mobileTraectoryAnimator] as const;
}
