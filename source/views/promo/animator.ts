import { progress } from './helpers';

type BooleanOrBooleanGetter = boolean | (() => boolean);
export class Animation {
   totalDuration = 0;
   storage: any[] = [];
   wait(_wait: number, skip: BooleanOrBooleanGetter = false) {
      this.storage.push({
         type: 'wait',
         _wait,
         skip,
      });
      this.totalDuration += _wait;
      return this;
   }
   play(_a: (p: number) => void, duration: number, _offset: number = 0, skip: BooleanOrBooleanGetter = false, restrict: boolean = true) {
      this.storage.push({
         type: 'play',
         _a,
         duration,
         _offset,
         restrict,
         skip,
      });
      this.totalDuration += duration - _offset;
      return this;
   }
   once(from: () => void, to: () => void, reset?: (p1: () => void, p2: () => void) => void, skip: BooleanOrBooleanGetter = false, d = 0.1) {
      this.storage.push({
         type: 'once',
         from,
         to,
         reset,
         skip,
         d,
      });
      return this;
   }
   compile() {
      return new Animator(this);
   }
}
export class Animator {
   private animation: Animation;
   constructor(animation: Animation) {
      this.animation = animation;
   }
   private cache = {};
   frame(currP: number) {
      if (this.animation.totalDuration > 1) {
         console.warn('total duration of anim is gt 1', this.animation.totalDuration);
      }
      let acc = 0;
      // this.animation.storage.forEach((x) => console.log((typeof x.skip === 'function' && x.skip()) || x.skip));
      const toPlay = this.animation.storage.filter((x) => (typeof x.skip === 'function' && x.skip() === false) || x.skip === false);
      // console.log('Skipped', this.animation.storage.length - toPlay.length);
      toPlay.forEach((anim, i) => {
         // console.log(anim.skip)
         if (anim.type === 'wait') {
            acc += anim._wait;
         } else if (anim.type === 'play') {
            const p = progress(acc, acc + anim.duration, currP);
            if (anim.restrict) {
               if (currP >= acc && currP <= acc + anim.duration) anim._a(p);
               else if (this.cache[i] != null && this.cache[i] !== p && (p === 0 || p === 1)) {
                  anim._a(p);
               }
            } else anim._a(p);
            acc += anim.duration - anim._offset;
            if (acc > 1) console.error('acc is gt than 100', acc);
            this.cache[i] = p;
         } else if (anim.type === 'once') {
            if (!anim.released && currP > acc) {
               anim.to();
               anim.released = true;
            } else if (anim.released && currP <= acc) {
               anim.from();
               anim.released = false;
            }
         }
      });
      this.lastP = currP;
   }
   reset() {
      this.animation.storage.forEach((anim) => {
         if (anim.type === 'once' && anim.type === 'released') {
            if (anim.reset) anim.reset(anim.from, anim.to);
            else anim.from();
            anim.released = false;
         }
      });
   }
}
