export function progress(a: number, b: number, x: number) {
   const r = (x - a) / (b - a);
   return Math.max(0, Math.min(r, 1));
}
