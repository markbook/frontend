import { useAppStore } from '@/stores/AppStore';
import { Match } from 'solid-js';
import s from './Profile.module.scss';
export default function Profile() {
  const [{ user }] = useAppStore();
  if (!user) return <div>Нет данных</div>;
  return <div>{user?.firstName}</div>;
}
