import Traectory from '@/components/Traectory';
import { Course } from '@/components/Traectory/types';
import { createSignal } from 'solid-js';
import { SVGIcon } from '@/components/Icon';
import SmartSelector from '@/components/SmartSelector';
import { SelectableItem } from '@/components/SmartSelector/SmartSelector';
import Button from '@/components/Button';
import { useRoute } from 'solid-typefu-router5';
import { createStore } from 'solid-js/store';

import Input from '../landing/Input';

import DoubleArrowIcon from './double_left.svg';
import ShuffleIcon from './shuffle.svg';
import s from './TreaectorySelection.module.scss';
import { useNavigate } from 'solid-app-router';
const emptyCourse: Course = {
   nodes: [],
   closed: 0,
   title: 'Ваш путь',
};
export default function TraecotryConstructor() {
   const navigate = useNavigate();
   function goNext() {
      navigate('/workspace');
   }
   function shuffle() {}
   const [traectory, setTraectory] = createSignal<Course>(emptyCourse);
   const [state, setState] = createStore({
      startInfo: {
         edu: [
            {
               title: 'Среднее',
               selected: false,
            },
            {
               title: 'Cпециальное',
               selected: false,
            },
            {
               title: 'Высшее',
               selected: true,
            },
         ] as SelectableItem[],
      },
   });
   return (
      <div class={s.view}>
         <div class={s.panel}>
            <h3>Траектория</h3>
            <div class={s.title}>
               <SVGIcon icon={DoubleArrowIcon} class={s.arrows} />
               <div>Начало</div>
            </div>
            <div class={s.target}>
               Текущий уровень образования
               <div
                  style={{
                     'align-self': 'center',
                  }}
               >
                  <SmartSelector
                     state={state.startInfo.edu}
                     onSelect={(item, index) => {
                        const oldIndex = state.startInfo.edu.findIndex((x) => x.selected === true);
                        setState('startInfo', 'edu', oldIndex, 'selected', false);
                        setState('startInfo', 'edu', index, 'selected', true);
                     }}
                  />
               </div>
               <Input title="Направление" color="black" list="traction" class={s.input}>
                  <datalist id="traction">
                     <option value="Chocolate" />
                     <option value="Coconut" />
                     <option value="Mint" />
                     <option value="Strawberry" />
                     <option value="Vanilla" />
                  </datalist>
               </Input>
            </div>
            <div class={s.title}>
               <SVGIcon
                  icon={DoubleArrowIcon}
                  class={s.arrows}
                  style={{
                     transform: 'rotate(180deg)',
                  }}
               />
               Цель
            </div>
            <div class={s.target}>
               <Input title="Основное направление" color="black" class={s.input} />
               <Input title="Дополнительное" color="black" class={s.input} />
            </div>
         </div>
         <div class={s.tracking}>
            <Traectory traectory={traectory()} zoomable height="96vh" width="100%" zIndexZero minZoom={85} maxZoom={85} />
         </div>
         <Button mode="reverse" onClick={() => goNext()}>
            Далее
         </Button>
         <SVGIcon
            icon={ShuffleIcon}
            style={{
               position: 'absolute',
               width: '55px',
               bottom: '50px',
               right: '50px',
               cursor: 'pointer',
            }}
            onClick={() => shuffle()}
         />
      </div>
   );
}
