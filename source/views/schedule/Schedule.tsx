import PhotoBadge from '@/components/Profile/PhotoBadge/PhotoBadge';
import { startOfToday, addMonths, addDays, setMonth, setDate, parse, isValid } from 'date-fns';
import { dateFnsFormat } from '@/helpers';

import AnimatedList from '@/components/AnimatedList';

import PointerSign from './assets/PointerSign.svg';
import { capitalize, getMonthName, monthNameToNumber } from './helpers';
import s from './Schedule.module.scss';
import { createComputed, createSignal, PropsWithChildren } from 'solid-js';
import { useNavigate, useParams } from 'solid-app-router';

export default function Schedule(props: PropsWithChildren) {
   const DATE_FORMAT = 'dd-MM-yyyy';
   
   const navigate = useNavigate();
   const params = useParams();

   let routeDate = parse(params.date, DATE_FORMAT, new Date());
   if (!isValid(routeDate)) routeDate = startOfToday();

   const [selectedDate, setSelectedDate] = createSignal(routeDate);

   createComputed(() => {
      navigate('/schedule/' + dateFnsFormat(selectedDate(), DATE_FORMAT), {
         replace: true,
      });
   });

   function generateMonthsSequence() {
      let months = [];
      for (let i = -2; i < 3; i++) months.push(capitalize(getMonthName(addMonths(selectedDate(), i))));
      return months;
   }
   function generateDaysSequence() {
      let days = [];
      for (let i = -4; i < 5; i++) days.push(addDays(selectedDate(), i).getDate());
      return days;
   }
   function selectMonth(monthName: string) {
      const monthNumber = monthNameToNumber(monthName);
      if (monthNumber < 0) throw new Error('Выбран неправильный месяц');
      setSelectedDate(setMonth(selectedDate(), monthNumber));
   }
   function selectDayOfMonth(day: number) {
      setSelectedDate(setDate(selectedDate(), day));
   }
   return (
      <div class={s.page}>
         <div class={s.title}>Раписание</div>
         <div class={s.views}>
            <div class={s.main}>
               <div class={s.months}>
                  <AnimatedList getData={generateMonthsSequence} animationDuration={200}>
                     {(month: string) => (
                        <div
                           class={s.month}
                           classList={{
                              [s.current]: month.toLocaleLowerCase() === getMonthName(startOfToday()),
                           }}
                           onClick={() => selectMonth(month)}
                        >
                           {month}
                        </div>
                     )}
                  </AnimatedList>
               </div>
               <div class={s.pointers}>
                  <img src={PointerSign} class={s.pointer} />
                  <img src={PointerSign} classList={{ [s.pointer]: true, [s.down]: true }} />
               </div>
               <div class={s.days}>
                  <AnimatedList animationDuration={200} getData={generateDaysSequence}>
                     {(day: number) => (
                        <div
                           title={dateFnsFormat(setDate(selectedDate(), day), 'dd MMMM yyyy')}
                           class={s.day}
                           classList={{
                              [s.current]: day === startOfToday().getDate(),
                           }}
                           onClick={() => selectDayOfMonth(day)}
                        >
                           {day}
                        </div>
                     )}
                  </AnimatedList>
               </div>
               {props.children}
            </div>
            <div class={s.secondary}>
               <PhotoBadge fullName="Евстрахова Мария Васильевна" email="M_V_teacher@gmail.com" />
            </div>
         </div>
      </div>
   );
}
