import LessonBar from '@/components/LessonBar';
import { dateFnsFormat } from '@/helpers';
import { For } from 'solid-js';
import { useRoute } from 'solid-typefu-router5';

import s from './ScheduleGrid.module.scss';
const schedule: any[] = [];
type ScheduleGridProps = {
  date: string;
};
export default function ScheduleGrid(props: ScheduleGridProps) {
  const route = useRoute();
  return (
    <div class={s.table}>
      <For each={schedule}>
        {(lesson, i) => (
          <div class={s.row}>
            <div class={s.time}>{dateFnsFormat(lesson.date, 'h:mm')}</div>
            <div class={s.moment}>
              <For each={new Array(Math.floor(lesson.duration / 45) * 2).fill(0)}>
                {() => <div class={s.cell} />}
              </For>
              <LessonBar
                lesson={lesson}
                marked={i() % 2 === 1}
                containerProps={{
                  class: s.lessonbar,
                }}
              />
            </div>
          </div>
        )}
      </For>
      {props.date}
    </div>
  );
}
