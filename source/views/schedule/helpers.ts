export function capitalize(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}
export function getMonthName(date: Date) {
  return date.toLocaleString('ru', {
    month: 'long',
  });
}
const monthes = [
  'Январь',
  'Февраль',
  'Март',
  'Апрель',
  'Май',
  'Июнь',
  'Июль',
  'Август',
  'Сентябрь',
  'Октябрь',
  'Ноябрь',
  'Декабрь',
];
export function monthNameToNumber(name: string) {
  return monthes.indexOf(name);
}
