import s from './Error.module.scss';
type ErrorProps = {
  errorText?: string;
};
export default function Error(props: ErrorProps) {
  return (
    <div>
      Произошла фатальная ошибка. Информация была отправлена и мы работаем над тем, чтобы это
      исправить. Приносим извинения.
      <div>{props.errorText}</div>
    </div>
  );
}
