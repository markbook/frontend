import Card from '@/components/Card';
import { ComponentProps } from 'solid-js';
import { addDays, addHours, addMinutes, getDate, startOfToday } from 'date-fns';
import { dateFnsFormat } from '@/helpers';
import cc from 'classcat';

function formatTime(date: Date) {
  return dateFnsFormat(date, 'HH:mm');
}
import s from './Schedule.module.scss';
// временно
type Lesson = {
  name: string;
  type: 'Лекция' | 'Практика';
  order: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | number;
};
type ScheduleProps = {} & ComponentProps<typeof Card>;
export default function Schedule({ class: cl, ...other }: ScheduleProps) {
  const date = addHours(startOfToday(), 8.5);
  // временно
  const lessonDurationMin = 90;
  // временно

  let уроки = [
    {
      name: 'Операционные системы',
      type: 'Практика',
    },
    {
      name: 'Data Base(||)',
      type: 'Практика',
    },
    {
      name: 'Математическое моделирование',
      type: 'Лекция',
    },
    {
      name: 'Математическое моделирование',
      type: 'Практика',
    },
    {
      name: 'Б.Ж.Д',
      type: 'Практика',
    },
    {
      name: 'Приложение системной инженерии',
      type: 'Лекция',
    },
  ] as Lesson[];
  уроки = уроки.map((x, i) => ({ ...x, order: i + 1 }));
  return (
    <Card {...other} class={cc([s.schedule, cl])}>
      <div class={s.calendar}>
        <div class={cc([s.digits, s.previous])}>{getDate(addDays(date, -1))}</div>
        <div class={s.current}>
          <div class={cc([s.digits, s.today])}>{getDate(date)}</div>
          <div class={s.month}>{dateFnsFormat(date, 'MMMM')}</div>
        </div>
        <div class={cc([s.digits, s.next])}>{getDate(addDays(date, 1))}</div>
      </div>
      <div class={s.subjects}>
        {уроки.map(({ name, type, order }) => (
          <div class={s.subject}>
            <div class={s.time}>
              {formatTime(addMinutes(date, (90 + 15) * (order - 1)))}
              {' - '}
              {formatTime(addMinutes(date, 90 * order + 15 * (order - 1)))}
            </div>
            <div
              class={cc([
                s.block,
                {
                  [s.practice]: type === 'Практика',
                  [s.lection]: type === 'Лекция',
                },
              ])}
            >
              <div class={s.type}>{type === 'Лекция' ? 'Лекция' : 'Лаб.'}</div>
              <div class={s.name}>{name}</div>
            </div>
          </div>
        ))}
      </div>
    </Card>
  );
}
