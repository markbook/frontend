import { createComputed, createEffect, createMemo, createSignal, JSX, lazy, onCleanup } from 'solid-js';
import Task from '@/components/Task';
import Helmet from '@/components/Helmet';
import cc from 'classcat';

const Traectory = lazy(() => import('@/components/Traectory'));
const Schedule = lazy(() => import('./Schedule'));
import { useAppStore } from '@/stores/AppStore';
import { dateFnsFormat } from '@/helpers';
import { differenceInHours } from 'date-fns';

import Avatar from '../../components/Task/Avatar.png';

import s from './Workspace.module.scss';
import LessonBar from '@/components/LessonBar';
import Segments from '@/components/Segments/Segments';
import { MonacoEditor } from '@/components/CodeEditor';
import { sample } from '../../views/experiments/test';

export default function Workspace() {
   const [offset, setOffset] = createSignal(0);
   const [store] = useAppStore();
   const [selectedDate, setSelectedDate] = createSignal(new Date(Date.now()));
   function onStart() {
      document.addEventListener('touchmove', move);
   }
   function move(e: TouchEvent) {
      // console.log(e.touches[0].clientX);
      console.log(offset() - e.touches[0].clientX);
      setOffset(e.touches[0].clientX);
      // setOffset();
   }
   function onEnd() {
      document.removeEventListener('touchstart', move);
   }
   onCleanup(() => onEnd());
   return (
      <>
         <Helmet>
            <title>Маркбук / Панель</title>
         </Helmet>
         <div class={s.workspace} onTouchStart={onStart}>
            <div class={cc([s.block, s.control])}>
               <h1>Маркбук</h1>
               <div class={s.menu}>
                  <div class={s.first}>
                     <div class={s.avatar}>
                        <div class={s.circle}>
                           <img src={Avatar} alt="" />
                        </div>
                     </div>
                     <div class={s.info}>
                        <div class={s.name}>
                           {store.user?.firstName} {store.user?.lastName}
                        </div>
                        <div class={s.group}>
                           <div>Уральский Федеральный Университет</div>
                           {store.user?.currentGroup}
                        </div>
                     </div>
                  </div>
                  <div class={s.top} />
                  <div class={s.bottom} />
               </div>
               <Traectory traectory={sample} maxZoom={60} fillWidth height="100vh" />
            </div>
            <div class={cc([s.block, s.task])}>
               <Task />
               <Tool />
            </div>
            <div class={cc([s.block, s.schedule])}>
               <div class={s.day}>{dateFnsFormat(selectedDate(), 'dd')}</div>
               <div class={s.month}>{dateFnsFormat(selectedDate(), 'LLLL')}</div>
               {store.schedule
                  ?.filter((x) => differenceInHours(x.eventDate, selectedDate()) < 12)
                  .map((x) => (
                     <LessonBar lesson={x} />
                  ))}
            </div>
         </div>
      </>
   );
}
function Tool() {
   createEffect(() => (window.onbeforeunload = () => {}));
   const [selectedTool, setSelectedTool] = createSignal(0);
   const tools: [JSX.Element, string][] = [
      [<MonacoEditor />, 'Текст/Код'],
      ...[
         ['https://www.geogebra.org/calculator', 'Геометрия'],
         ['https://www.wolframalpha.com/', 'Алгебра'],
         ['https://desmos.com/calculator', 'Графики'],
      ].map((x) => [<iframe src={x[0]} frameborder="0" width="100%" height="100%" />, x[1]] as [JSX.Element, string]),
   ];
   return (
      <div class={s.tool}>
         {/* <MonacoEditor />
         {tools.map(([tool, nname], index) => {
            let el = tool as HTMLElement;
            if (typeof el === 'function') el = tool();
            el.style.display = selectedTool() === index ? 'initial' : 'none';
            console.log(el);
            return el;
         })} */}
         {tools[selectedTool()][0]}
         <Segments
            options={tools.map((x) => x[1])}
            selectedIndex={selectedTool()}
            onSelect={(i) => setSelectedTool(i)}
         />
      </div>
   );
}
