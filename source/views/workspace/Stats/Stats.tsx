// import SolidChart, { SolidChartProps } from 'solid-chart.js';

import { createStore } from 'solid-js/store';

import s from './Stats.module.scss';
const edges = 6;
const labels = ['Solid', 'Is', 'Most', 'Performant', 'JavaScript', 'Framework'];
const datasets = new Array<number[]>(edges).fill([]).map((_, i) => {
   const arr = [0, 0, 0, 0, 0, 0];
   if (i === edges - 1) {
      arr[i] = 60;
      arr[0] = 60;
   } else {
      arr[i] = 60;
      arr[i + 1] = 60;
   }
   return {
      backgroundColor: i % 2 === 0 ? '#3164A3' : 'white',
      data: arr,
      label: labels[i],
   };
});
export default function Stats() {
   const settings: SolidChartProps = {
      type: 'radar',
      data: {
         labels,
         datasets,
      },
      options: {
         responsive: false,
         legend: {
            display: false,
         },
         title: {
            display: true,
            text: 'SOLID CHART',
         },
         scale: {
            ticks: {
               display: false,
               maxTicksLimit: edges,
            },
         },
      },
   };
   const [chart, setChart] = createStore(settings);
   return (
      <div class={s.stats}>
         {/* <SolidChart
        {...chart}
        canvasOptions={
          {
            // width: '500',
            // height: '500',
          }
        }
      /> */}
      </div>
   );
}
