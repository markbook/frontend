import Card from '@/components/Card';
import cc from 'classcat';
import { ComponentProps, createRenderEffect, createSignal } from 'solid-js';
import { SVG } from '@svgdotjs/svg.js';

import s from './Tracking.module.scss';
type TrackingProps = {} & ComponentProps<typeof Card>;

type StopProps = {
  title: string;
};
function Stop({ title }: StopProps) {
  const [container, setContainer] = createSignal<HTMLDivElement>();
  createRenderEffect(() => {
    const el = container();
    if (!el) return;
    SVG().addTo(el);
    console.log('stop created');
  });
  return <div class={s.stop} ref={(d: HTMLDivElement) => setContainer(d)}></div>;
}
export default function Tracking({ class: cl, ...cardProps }: TrackingProps) {
  const [container, setContainer] = createSignal<HTMLDivElement>();
  createRenderEffect(() => {
    const el = container();
    if (!el) return;
    const distance = 90;
    const radius = 13;
    const steps = 7;
    const current = 3;
    const draw = SVG()
      .addTo(el)
      .size(150, distance * (steps - 1) + radius * steps);
    const xLineOffset = radius / 2;

    for (let i = 0; i < steps; i++) {
      const pointColor = i > current ? 'white' : i == current ? 'green' : '#0090ff';
      const lineColor = i > current ? 'white' : '#0090ff';
      const step = distance * i;
      draw
        .circle(radius)
        .attr({
          fill: pointColor,
        })
        .move(0, step);
      draw
        .plain(`Stop ${i + 1}`)
        .font({
          size: 14,
        })
        .move(radius * 1.5, step);
      if (i > 0) {
        const prevStep = distance * (i - 1);
        draw.line(xLineOffset, prevStep + radius * 2, xLineOffset, step - radius).stroke({
          color: lineColor,
          width: radius * 0.8,
          linecap: 'round',
        });
        // draw.defs
      }
    }
  });
  function createContainer(el: HTMLDivElement) {
    setTimeout(() => setContainer(el));
  }
  return (
    <Card {...cardProps} class={cc([cl, s.tracking])}>
      <div class="drawing" ref={createContainer} />
    </Card>
  );
}
