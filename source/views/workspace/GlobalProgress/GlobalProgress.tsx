import { ComponentProps } from 'solid-js';
import cc from 'classcat';

import s from './GlobalProgress.module.scss';
type GlobalProgressProps = {} & ComponentProps<'div'>;
export default function GlobalProgress(props: GlobalProgressProps) {
  return (
    <div {...props} class={cc(props.class, s.progress)}>
      Progress
    </div>
  );
}
