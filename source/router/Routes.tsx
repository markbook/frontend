import Promo from '@/views/promo';
import Experiments from '@/views/experiments/Experiments';
import Workspace from '@/views/workspace';
import TraecotryConstructor from '@/views/traectory_selection/TraectoryConstructor';
import Schedule from '@/views/schedule';
import Courses from '@/views/courses';
import Profile from '@/views/profile';
import { RouteDefinition } from 'solid-app-router';

export const routes: RouteDefinition[] = [
   {
      path: '/',
      component: Promo,
   },
   {
      path: '/promo',
      component: Promo,
   },
   // {
   //    path: '/traectory-selection',
   //    component: TraecotryConstructor,
   // },
   // {
   //    path: '/experiments',
   //    component: Experiments,
   // },
   // {
   //    path: '/workspace',
   //    component: Workspace,
   // },
   // {
   //    path: '/schedule',
   //    component: Schedule,
   // },
   // {
   //    path: '/schedule/:date',
   //    component: Schedule,
   // },
   // {
   //    path: '/courses',
   //    component: Courses,
   // },
   // {
   //    path: '/profile',
   //    component: Profile,
   // },
   // {
   //    path: '/*all',
   //    component: Promo,
   // },
];
