import { Lesson } from '@/types/entities';
import { dateFnsFormat, dateFnsFormatDuration } from '@/helpers';
import { ComponentProps } from 'solid-js';
import cc from 'classcat';

import PorfileIcon from './assets/Profile.svg';
import s from './LessonBar.module.scss';
import { ScheduleObject } from '@/api/API';

type LessonBarProps = {
  lesson: ScheduleObject;
  displayTime?: boolean;
  containerProps?: ComponentProps<'div'>;
  marked?: boolean;
};
export default function LessonBar({ lesson, containerProps, displayTime, marked }: LessonBarProps) {
  return (
    <div
      class={cc([s.lessonbar, containerProps?.class])}
      className={s[`x-${Math.floor(lesson.duration / 45)}`]}
    >
      <div classList={{ [s.marker]: true, [s.marked]: marked }} />
      <div class={s.content}>
        <div class={s.main}>
          {displayTime && <div>{dateFnsFormat(lesson.date, 'hh:mm')}</div>}
          <b>{lesson.name}</b>
          <div>{lesson.description}</div>
        </div>
        <div class={s.secondary}>
          <div>{lesson.teacher.fullName}</div>
          <div class={s.profile} innerHTML={PorfileIcon} />
          <div>{dateFnsFormatDuration({ minutes: lesson.duration })}</div>
        </div>
      </div>
    </div>
  );
}
