export { default as MonacoEditor } from './Monaco';
export { default as CodeMirrorEditor } from './CodeMirror';
export type EditorType = 'monaco' | 'codemirror';
