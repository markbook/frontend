import { createEffect, ComponentProps } from 'solid-js';
import * as monaco from 'monaco-editor';

import { csharp } from '../defaultDocs';

import editorWorker from 'monaco-editor/esm/vs/editor/editor.worker?worker';
import jsonWorker from 'monaco-editor/esm/vs/language/json/json.worker?worker';
import cssWorker from 'monaco-editor/esm/vs/language/css/css.worker?worker';
import htmlWorker from 'monaco-editor/esm/vs/language/html/html.worker?worker';
import tsWorker from 'monaco-editor/esm/vs/language/typescript/ts.worker?worker';
import { splitProps } from 'solid-js';

type MonacoProps = {} & ComponentProps<'div'>;
(self as any).MonacoEnvironment = {
   getWorker(_: any, label: string) {
      if (label === 'json') {
         return new jsonWorker();
      }
      if (label === 'css' || label === 'scss' || label === 'less') {
         return new cssWorker();
      }
      if (label === 'html' || label === 'handlebars' || label === 'razor') {
         return new htmlWorker();
      }
      if (label === 'typescript' || label === 'javascript') {
         return new tsWorker();
      }
      return new editorWorker();
   },
};
export default function Monaco(props: MonacoProps) {
   let container: HTMLDivElement;
   let editor: monaco.editor.IStandaloneCodeEditor;
   createEffect(() => {
      setTimeout(() => {
         editor = monaco.editor.create(container, {
            value: csharp,
            language: 'csharp',
            lineNumbers: 'off',
            roundedSelection: false,
            scrollBeyondLastLine: false,
            readOnly: false,
            automaticLayout: true,
            theme: 'vs-dark',
            minimap: {
               enabled: false,
            },
         });
      });
   });
   const [local, other] = splitProps(props, []);
   return <div ref={(r: HTMLDivElement) => (container = r)} {...other}></div>;
}
