import { basicSetup } from '@codemirror/next/basic-setup';
import { cpp } from '@codemirror/next/lang-cpp';
import { EditorState, EditorStateConfig } from '@codemirror/next/state';
import { EditorView, keymap } from '@codemirror/next/view';
import { ComponentProps, createEffect, mergeProps } from 'solid-js';
import { defaultKeymap } from '@codemirror/next/commands';

import { csharp } from '../defaultDocs';

type CodeEditorProps = {
   config?: EditorStateConfig;
   divProps?: ComponentProps<'div'>;
};
const defaultEditorConfig: CodeEditorProps = {
   config: {
      extensions: [keymap(defaultKeymap), cpp(), basicSetup],
      doc: csharp,
   },
};
export default function CodeMirror(props: CodeEditorProps) {
   props = mergeProps(defaultEditorConfig, props);
   let parent: HTMLDivElement;
   let state: EditorState;
   let view: EditorView;
   createEffect(() => {
      state = EditorState.create(props.config);
      view = new EditorView({ state, parent });
   });
   return <div ref={(r: HTMLDivElement) => (parent = r)} {...props.divProps}></div>;
}
