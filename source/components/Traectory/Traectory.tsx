import { batch, createEffect, createMemo, createSignal, mergeProps, onCleanup, onMount } from 'solid-js';

import CourseNode from './CourseNode';
import { buildGraph } from './algo';
import { Course } from './types';
type TraectoryProps = {
   traectory: Course;
   fillHeight?: boolean;
   fillWidth?: boolean;

   width?: number | string;
   height?: number | string;

   zoomable?: boolean;
   minZoom?: number;
   maxZoom?: number;
   zIndexZero?: boolean;

   rotate?: number;
};
export default function Traectory(props: TraectoryProps) {
   let svgRef: SVGSVGElement;
   let point: DOMPoint;
   let pointerOrigin: DOMPoint;
   let isPointerDown = false;
   props = mergeProps(
      {
         minZoom: 40,
         maxZoom: 400,
         rotate: 0,
      },
      props
   );
   const node = createMemo(() => buildGraph(props.traectory));
   const [x, setX] = createSignal(-node().width.left * 500);
   const [y, setY] = createSignal(-node().width.left * 400);

   const [size, setSize] = createSignal(props.maxZoom!);

   onMount(() => {
      point = svgRef.createSVGPoint();

      if (props.fillWidth || props.fillHeight) {
         updateSize();
         window.addEventListener('resize', updateSize);
         onCleanup(() => window.removeEventListener('resize', updateSize));
      }
   });
   function updateSize() {
      if (svgRef.parentElement) {
         const parentRect = svgRef.parentElement.getBoundingClientRect();
         if (props.fillWidth) {
            svgRef.setAttribute('width', parentRect.width + 'px');
         } else if (props.fillHeight) {
            svgRef.setAttribute('height', parentRect.height + 'px');
         }
      }
   }
   function getPointFromEvent(event: MouseEvent) {
      if (!svgRef) throw new Error('svg ref not inited');
      point.x = event.clientX;
      point.y = event.clientY;
      const invertedSVGMatrix = svgRef.getScreenCTM()?.inverse();
      return point.matrixTransform(invertedSVGMatrix);
   }
   function onPointerDown(event: MouseEvent) {
      isPointerDown = true;
      pointerOrigin = getPointFromEvent(event);
      console.log('down');
   }
   function onPointerUp() {
      isPointerDown = false;
   }
   function onPointerMove(event: MouseEvent) {
      if (!isPointerDown) {
         return;
      }
      event.preventDefault();
      const pointerPosition = getPointFromEvent(event);
      batch(() => {
         setX(Math.round(x() - (pointerPosition.x - pointerOrigin.x)));
         setY(Math.round(y() - (pointerPosition.y - pointerOrigin.y)));
      });
   }
   function onWheel(ev: WheelEvent) {
      if (!props.zoomable) return;

      ev.preventDefault();
      let scale = size() + ev.deltaY * 0.1;
      scale = Math.round(Math.min(Math.max(scale, props.minZoom!), props.maxZoom!));
      if (size() !== scale) setX(Math.round(x() - ev.deltaY * 0.05));
      setSize(scale);
   }
   createEffect(() => console.log(node()));
   return (
      <svg
         style={{
            position: 'absolute',
            left: '0px',
            top: '15px',
            'z-index': props.zIndexZero ? 0 : -1,
         }}
         ref={(r) => (svgRef = r)}
         viewBox={`${x()} ${y()} ${size()} ${size()}`}
         onMouseDown={onPointerDown}
         onMouseUp={onPointerUp}
         onMouseLeave={onPointerUp}
         onMouseMove={onPointerMove}
         onWheel={onWheel}
         width={props.width}
         height={props.height}
      >
         <CourseNode course={node()} root rotate={props.rotate} />
      </svg>
   );
}
