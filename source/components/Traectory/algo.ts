import { tan } from './config';
import { Course, Node } from './types';

export function randomColor()
{
   return (
      '#' +
      Math.round(0x1000000 + 0xffffff * Math.random())
         .toString(16)
         .slice(1)
   );
}
export function buildGraph(node: Course)
{
   return offsets(levels(relocate(size(node))));
}
const NODE_MIN_LENGTH = 1;
/**
 * Вычисляем веса и ширину каждой ноды рекурсивно
 */
function size(course: Course)
{
   let leftSum = 0;
   let rightSum = 0;

   let leftWidth = 0;
   let rightWidth = 0;
   const node: Node = {
      ...course,
      weight: {
         left: 0,
         right: 0,
         total: course.nodes ? 1 : 0,
      },
      width: {
         left: 0,
         right: 0,
         total: course.nodes ? 1 : 0,
      },
      dir: 'none',
      level: 0,
      length: course.nodes ? Math.max(course.nodes.length, NODE_MIN_LENGTH) : 0,
      offset: 0,
      color: course.color,
   };
   if (!node.color && course.nodes) node.color = randomColor();
   node.nodes = course.nodes?.map((_n, i) =>
   {
      const n = size(_n);

      // равномерно распределяем расположения дочерних нод
      if (n.nodes)
      {
         if (leftSum + n.weight.total > rightSum + n.weight.total)
         {
            rightSum += n.weight.total;
            n.dir = 'right';
            rightWidth = Math.max(rightWidth, n.width.total);
         } else
         {
            leftSum += n.weight.total;
            n.dir = 'left';
            leftWidth = Math.max(leftWidth, n.width.total);
         }
      }
      else
      {
         n.dir = 'none';
      }
      n.parent = node;
      if (!n.color)
      {
         n.color = node.color;
      }
      return n;
   });
   node.weight.total += leftSum + rightSum;
   node.weight.left = leftSum;
   node.weight.right = rightSum;

   node.width.total += leftWidth + rightWidth;
   node.width.left = leftWidth;
   node.width.right = rightWidth;
   return node;
}
/**
 * Переворачиваем вокруг свои оси ноды так, чтобы они смотрели тяжелой стороной против центральной оси
 */
function relocate(node: Node, parentDir: 'left' | 'right' | 'none' | null = null)
{
   node.nodes?.forEach((n) => relocate(n, parentDir ?? n.dir));
   if (!parentDir) return node;
   if ((parentDir === 'right' && node.weight.left > node.weight.right) || (parentDir === 'left' && node.weight.right > node.weight.left))
   {
      node.nodes?.forEach((n) =>
      {
         if (n.dir === 'left') n.dir = 'right';
         else if (n.dir === 'right') n.dir = 'left';
      });
      let _ = node.weight.left;
      node.weight.left = node.weight.right;
      node.weight.right = _;

      _ = node.width.left;
      node.width.left = node.width.right;
      node.width.right = _;
   }
   return node;
}
// вычисляем настоящие уровни (расположение нод по горизонтали) относительно главной оси
function levels(node: Node, lvl = 0)
{
   if (lvl === 0)
   {
      node.nodes?.forEach((n) =>
      {
         if (n.dir === 'left')
         {
            levels(n, -n.width.right - 1);
         }
         else if (n.dir === 'right')
         {
            levels(n, n.width.left + 1);
         }
      });
   }
   else
   {
      if (node.nodes)
      {
         node.nodes.forEach((n) =>
         {
            let newLvl = lvl;
            if (n.dir === 'left')
            {
               newLvl = lvl - 1 - n.width.right;
            }
            else if (n.dir === 'right')
            {
               newLvl = lvl + 1 + n.width.left;
            }
            levels(n, newLvl);
         });
      }
   }
   node.level = lvl;
   return node;
}
// вычисляем полную максимальную длину ноды с зависимостями
function getTotalLength(node: Node)
{
   if (!node.nodes) return 0;
   let max = -Infinity;
   node.nodes.forEach((n) =>
   {
      if (n.nodes)
      {
         const length = getTotalLength(n);
         const dOffset = n.offset - node.offset;
         max = Math.max(max, length + dOffset);
      }
   });
   max = Math.max(node.length, max);
   return max;
}
/**
 * Устанавливаем отсупы (по вертикали) от соседних нод, чтобы избежать перекрывания
 * @param node
 * @param offset
 * @returns
 */
function offsets(node: Node, offset = 0)
{
   let maxOffset = 0;
   if (node.nodes)
   {
      let globalNodesOffset = 1;
      if (node.parent) globalNodesOffset = Math.abs(node.parent.level - node.level);

      for (let i = 0; i < node.nodes.length; i++)
      {
         const n = node.nodes[i];
         let _offset = globalNodesOffset + offset + i + 1;
         let prevAtSameDir: Node | null = null;
         let j = i - 1;
         while (j >= 0)
         {
            if (node.nodes[j].dir === n.dir)
            {
               prevAtSameDir = node.nodes[j];
               break;
            }
            j--;
         }
         if (prevAtSameDir)
         {
            const prevTotalLength = getTotalLength(prevAtSameDir);
            if (prevAtSameDir.offset + prevTotalLength + 1 > _offset)
            {
               _offset = prevAtSameDir.offset + prevTotalLength + 1;
            }
         }
         if (_offset > maxOffset) maxOffset = _offset;
         offsets(n, _offset);
      }
      const length = maxOffset - offset;
      if (length > node.length)
      {
         node.length = length;
      }
      if (offset === 0)
      {
         console.log(node.length);
         node.nodes?.forEach(n => maxOffset < getTotalLength(n) + n.offset ? maxOffset = getTotalLength(n) + n.offset : null);
         node.length = maxOffset + 2;
      }
   }
   node.offset = offset;
   return node;
}
