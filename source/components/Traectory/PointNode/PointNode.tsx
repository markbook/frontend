import { createMemo, mergeProps } from 'solid-js';

import { POINT_RADIUS, POINT_STROKE } from '../config';

type StopProps = {
   cx: number;
   cy: number;
   color: string;
   radius?: number;
};
export default function PointNode(props: StopProps) {
   props = mergeProps(
      {
         radius: POINT_RADIUS,
      },
      props
   );
   // учитываем, что stroke в svg идет по центру линии
   const stroke = createMemo(() => props.radius! * 0.4);
   return (
      <circle
         cx={props.cx}
         cy={props.cy}
         r={props.radius}
         fill="white"
         
         stroke={props.color}
         stroke-width={POINT_STROKE}
         style={{
            cursor: 'pointer',
            
         }}
      />
   );
}
