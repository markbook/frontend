import { createMemo, For, Match, Show, Switch, mergeProps, splitProps } from 'solid-js';

import ChapterNode from '../ChapterNode';
import { LINE_MARGIN, LINE_STROKE_WIDTH, LINE_WIDTH, POINT_RADIUS, tan, xMultiplier, yMultiplier } from '../config';
import PointNode from '../PointNode';
import { Node } from '../types';

import s from './CourseNode.module.scss';
import { Schevrones } from './SchevronesProps';

type CourseNodeProps = {
   course: Node;
   root?: boolean;
   rotate?: number;
};
const bottomTriangleAngle = 45;
const bottomSpike = (Math.tan((bottomTriangleAngle * Math.PI) / 180) * LINE_WIDTH) / 2;

const rotate = (x: number, y: number, a: number) => ({
   x: x * Math.cos(a) - y * Math.sin(a),
   y: x * Math.sin(a) + y * Math.cos(a),
});

export default function CourseNode(props: CourseNodeProps) {
   props = mergeProps(
      {
         rotate: 0,
      },
      props
   );
   const [commonProps] = splitProps(props, ['rotate']);

   const coord = (x: number, y: number) => rotate(x, y, props.rotate!);

   const _Sx = createMemo(() => {
      let value = props.course.level;
      let dir = 0;
      if (props.course.dir === 'left') {
         value += props.course.width.right + 1;
         dir = -1;
      } else if (props.course.dir === 'right') {
         value -= props.course.width.left + 1;
         dir = 1;
      }
      value *= xMultiplier;
      return value + LINE_MARGIN * dir;
   });

   const _Sy = createMemo(() => props.course.offset * yMultiplier);

   const S = createMemo(() => ({ x: _Sx(), y: _Sy() }));
   const adjustedS = createMemo(() => coord(_Sx(), _Sy()));

   const dx = createMemo(() => props.course.level * xMultiplier - S().x);
   const dy = createMemo(() => props.course.length * yMultiplier);
   const dir = createMemo(() => Math.sign(dx()) || -1);

   const yFinal = createMemo(() => (!props.root ? tan * Math.abs(dx()) : LINE_WIDTH / 2));

   // Using absolute coord for being able to transform
   const path = () => {
      if (props.root) {
         /*      S
         /\
      A /  \ E
        |  |
        |  |
        |  |
        |  | 
        |  |
        | C|
        |/\|
        B   D
        
          ^
        bottom
*/
         const Ax = S().x - LINE_WIDTH / 2;
         const Ay = S().y + LINE_WIDTH / 2;

         const Bx = Ax;
         const By = Ay + dy();

         const Cx = Bx - (dir() * LINE_WIDTH) / 2;
         const Cy = By - bottomSpike;

         const Dx = Cx - (dir() * LINE_WIDTH) / 2;
         const Dy = Cy + bottomSpike;

         const Ex = Dx;
         const Ey = Dy - dy();

         const points = [coord(Ax, Ay), coord(Bx, By), coord(Cx, Cy), coord(Dx, Dy), coord(Ex, Ey)].map((p) => `L${p.x},${p.y}`).join(' ');

         return `M${S().x},${S().y} ${points} Z`;
      }
      /*
   A
 S |\
   | \
 G \  \
    \  \ 
     \  \   } edge 1
      \  \
       \  \ B   
      F |  | 
edge 2 {|  |
        | D|
        |/\|
        E   C
        
          ^
        bottom
*/
      const Ax = S().x + dir() * (LINE_WIDTH / 2);
      const Ay = S().y - LINE_WIDTH / 2;
      const A = coord(Ax, Ay);

      const Bx = Ax + dx();
      const By = Ay + yFinal();

      const Cx = Bx;
      const Cy = By + dy();

      const Dx = Cx + (-dir() * LINE_WIDTH) / 2;
      const Dy = Cy - bottomSpike;

      const Ex = Dx + (-dir() * LINE_WIDTH) / 2;
      const Ey = Dy + bottomSpike;

      const Fx = Ex;
      const Fy = Ey - dy() + LINE_WIDTH / 2;

      const lvl = (props.course.dir === 'left' ? props.course.width.right : props.course.width.left) + 1;
      // const angle = (Math.PI / 180) * (50 / Math.pow(lvl, 0.55));
      console.log(lvl);
      let angle;
      switch (lvl) {
         case 1:
            angle = 49;
            break;
         case 2:
            angle = 37;
            break;
         case 3:
            angle = 23;
            break;
         default:
            angle = 0;
            break;
      }
      angle = 90 - angle
      console.log(angle);
      angle *= Math.PI / 180;
      const Gx = Fx - dx() + dir() * LINE_WIDTH;
      const Gy = Fy - yFinal() + LINE_WIDTH + Math.sin(angle / 2) * LINE_WIDTH * Math.cos(angle);

      const AAx = Ax + dir() * LINE_WIDTH * Math.cos(angle) * Math.cos(angle / 2);
      const AAy = S().y + LINE_WIDTH * Math.cos(angle) * Math.sin(angle / 2);
      const AA = coord(AAx, AAy);
      const points = [coord(Bx, By), coord(Cx, Cy), coord(Dx, Dy), coord(Ex, Ey), coord(Fx, Fy), coord(Gx, Gy)]
         .map((p) => `L${p.x},${p.y}`)
         .join(' ');

      return `M${AA.x},${AA.y} ${points} Z`;
   };

   const pathId = props.course.title + Math.floor(Math.random() * dx() * dy());
   const textPath = createMemo(() =>
      dir() >= 0
         ? `M${S().x},${S().y - LINE_WIDTH / 4} l${dx()},${yFinal()} v${dy()}`
         : `M${S().y + dx()},${S().y + dy() + yFinal()} v${-dy() - LINE_WIDTH / 4} l${-dx()},${-yFinal()}`
   );
   return (
      <Switch>
         <Match when={!props.course.nodes}>
            <ChapterNode course={props.course} point={adjustedS()} />
         </Match>
         <Match when={props.course.nodes}>
            <path
               d={path()}
               fill="transparent"
               class={s.course}
               stroke={props.course.color}
               stroke-width={LINE_STROKE_WIDTH}
               stroke-linejoin="round"
               stroke-linecap="round"
            />
            <path
               // sweep-flag="0"
               visibility="hidden"
               // fill="transparent"
               // stroke="black"
               id={pathId}
               stroke-width={LINE_STROKE_WIDTH}
               d={textPath()}
            />
            {/* <text>
               <textPath
                  class={s.title}
                  href={'#' + pathId}
                  startOffset={dir() >= 0 ? '10%' : '60%'}
                  style="color: black; fill: black;"
                  dominant-baseline="middle"
                  // transform={dir() < 0 ? 'scale(+1,-1)' : undefined}
               >
                  {props.course.title}
               </textPath>
            </text> */}
            <Schevrones
               count={2}
               x={S().x + dx()}
               y={S().y + dy() + yFinal()}
               stroke={props.course.color}
               stroke-width={0.5}
               stroke-linejoin="round"
               stroke-linecap="round"
            />
            <Switch>
               <Match when={props.root}>
                  <Schevrones
                     count={2}
                     x={S().x}
                     y={S().y - 4.5}
                     stroke={props.course.color}
                     stroke-width={0.5}
                     stroke-linejoin="round"
                     stroke-linecap="round"
                  />
               </Match>
               <Match when={!props.root}>
                  <PointNode color={props.course.parent!.color} cx={S().x} cy={S().y} radius={POINT_RADIUS / 10} />
                  {/* <PointNode color={props.course.parent!.color} cx={adjustedS().x} cy={adjustedS().y} radius={POINT_RADIUS} /> */}
                  {/* <PointNode color={props.course.color} cx={adjustedS().x} cy={adjustedS().y} radius={POINT_RADIUS * 0.65} /> */}
               </Match>
            </Switch>
            <For each={props.course.nodes!}>{(c) => <CourseNode course={c} {...commonProps} />}</For>
         </Match>
      </Switch>
   );
}
