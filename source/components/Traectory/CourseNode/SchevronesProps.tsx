import { ComponentProps, For, mergeProps, splitProps } from 'solid-js';

type SchevronesProps = {
   x: number;
   y: number;
   count?: number;
} & ComponentProps<'path'>;
export function Schevrones(props: SchevronesProps)
{
   props = mergeProps(
      {
         count: 2,
      },
      props
   );
   const r = 2;
   const dp = 2;
   const [my, other] = splitProps(props, ['x', 'y', 'count']);
   return (
      <For each={Array(props.count).fill(0)}>
         {(_, i) => (
            <path
               d={`M${props.x - r},${props.y + i() * dp + r} l${r},${-r} l${r},${r}`}
               {...other}
               fill="transparent" />
         )}
      </For>
   );
}
