import { For } from 'solid-js';

import { POINT_RADIUS } from '../config';
import PointNode from '../PointNode';
import { Node } from '../types';

import s from './ChapterNode.module.scss';

type ChapterProps = {
   point: {
      x: number;
      y: number;
   };
   course: Node;
};
export default function ChapterNode(props: ChapterProps) {
   return (
      <>
         <PointNode cx={props.point.x} cy={props.point.y} color={props.course.color} />
         <For each={props.course.chapters!}>
            {(chapter, i) => {
               let angle = (i() / props.course.chapters!.length) * 2 * Math.PI;
               let color: string;
               switch (chapter.status) {
                  case 0:
                     color = '#979797';
                     break;
                  case 1:
                     color = '#FAFF00';
                     break;
                  case 2:
                     color = '#00FF19';
                     break;
                  default:
                     color = '#979797';
                     break;
               }
               return (
                  <circle
                     cx={props.point.x + Math.cos(angle) * POINT_RADIUS * 0.8}
                     cy={props.point.y + Math.sin(angle) * POINT_RADIUS * 0.8}
                     r={0.5}
                     fill={color}
                  />
               );
            }}
         </For>
         <text class={s.pointTitle} x={props.point.x - 5} y={props.point.y} dominant-baseline="middle" direction="rtl">
            {props.course.title}
         </text>
      </>
   );
}
