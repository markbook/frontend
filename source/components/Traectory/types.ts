export type Course = {
   title: string;
   nodes?: Course[];
   closed?: number;
   color?: string;
   chapters?: Chapter[];
};
type Chapter = {
   status: ChapterStatus;
};
enum ChapterStatus
{
   NotDone,
   InProgress,
   Done
}
export type Node = Omit<Course, 'nodes'> & {
   weight: {
      /**
       * Общее количество дочерних нод (включая вложенные)
       */
      total: number;
      /**
       * Общее количество дочерних нод слева
       */
      left: number;
      /**
       * Общее количество дочерних нод слева
       */
      right: number;
   };
   width: {
      total: number;

      left: number;

      right: number;
   };
   /**
    * Дочерние ноды
    * Если их нет, то эо точка
    */
   nodes?: Node[];
   /**
    * Цвет
    */
   color: string;
   /**
    * Расположение относительно родительской ноды
    */
   dir: 'left' | 'right' | 'none';
   /**
    * Длина по количетсву точек
    */
   length: number;
   /**
    * Уровень относительно центральной оси (может быть отрицательным)
    */
   level: number;
   /**
    * Дополнительный отступ сверху
    */
   offset: number;

   parent?: Node;
};
