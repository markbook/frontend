export class Task {
  title: string;
  description: string;
  constructor(title: string, description: string) {
    this.title = title;
    this.description = description;
  }
}
export class CodeTask extends Task {
  codeSource: string;
  output?: string;
  constructor(title: string, description: string, codeSource: string, output?: string) {
    super(title, description);
    this.title = title;
    this.description = description;
    this.codeSource = codeSource;
    this.output = output;
  }
}
export class SingleWritingTask extends Task {}
