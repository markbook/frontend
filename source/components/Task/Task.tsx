/* eslint-disable import/order */
import { createEffect, createSignal, For } from 'solid-js';

// const MonacoEditor = lazy(() => import('@/components/CodeEditor/Monaco'));
import { EditorType } from '../CodeEditor';

import { Task, CodeTask, SingleWritingTask } from './shims';
import s from './Task.module.scss';
type TaskProps = {
   editorType: EditorType;
   task: Task;
};

import AsciiMathParser from './asciimath2tex';
import katex from 'katex';
import 'katex/dist/katex.min.css';
import renderMathInElement from 'katex/dist/contrib/auto-render';
export default function (props: TaskProps) {
   let quiestionRef: HTMLParagraphElement;
   const [question, setQuestion] = createSignal(
      'Докажите, что отрезки $A A_1$, $B B_1$, $C C_1$ пересекаются в одной точке тогда и только тогда, когда $(A*B_1)/(B_1*C)*(C*A_1)/(A_1*B)*(B*C_1)/(C_1*A)$'
   );
   const maths = Array.from(question().matchAll(/\$(.*?)\$/gm));
   const parser = new AsciiMathParser();
   maths.forEach((x) => {
      setQuestion(question().replace(x[1], parser.parse(x[1])));
   });
   createEffect(() => {
      question();
      if (!quiestionRef) return;
      renderMathInElement(quiestionRef, {
         delimiters: [{ left: '$', right: '$', display: false }],
      });
   });
   return (
      <div class={s.card}>
         <div class={s.title}>
            <strong>Задание</strong> №53856
         </div>
         <div class={s.about}>
            Сложность
            <div class={s.level}>
               <For each={Array(5).fill(0)}>
                  {(_, i) => (
                     <div
                        class={s.mark}
                        classList={{
                           [s.filled]: i() < 3,
                        }}
                     />
                  )}
               </For>
            </div>
            <br />
            Источник{' '}
            <a href={'https://problems.ru'} target="blank">
               problems.ru
            </a>
         </div>
         <br />
         <u>Условие</u>
         <p>
            Пусть точки A<sub>1</sub>, B<sub>1</sub> и C<sub>1</sub> принадлежат сторонам соответственно BC, AC и AB
            треугольника ABC.
         </p>
         <u>Вопрос</u>
         <p ref={(r) => (quiestionRef = r)} innerHTML={question()} />
      </div>
   );
}
