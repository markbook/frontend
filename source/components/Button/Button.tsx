import { ComponentProps, JSX } from 'solid-js';
import s from './Button.module.scss';

type ButtonProps = {
  mode?: 'secondary' | 'reverse';
  transparent?: boolean;
  children?: JSX.Element;
} & ComponentProps<'button'>;
export default function Button({ mode, transparent, children, ...props }: ButtonProps) {
  return (
    <button
      class={s.button}
      className={mode && s[mode]}
      classList={{
        [s.transparent]: transparent,
      }}
      {...props}
    >
      {children}
    </button>
  );
}
