import cc from 'classcat';

import s from './Card.module.scss';
type CardProps = {} & JSX.HTMLAttributes<HTMLDivElement>;
export default function Card({ class: cl, children, ...other }: CardProps) {
  return (
    <div class={cc([s.card, cl])} {...other}>
      {children}
    </div>
  );
}
