import { Portal } from 'solid-js/web';
import { Component, ComponentProps } from 'solid-js';

export type HelmetProps = {} & Omit<ComponentProps<typeof Portal>, 'mount'>;

export default function Helmet({ children, ...portalProps }: HelmetProps) {
  return (
    <Portal mount={document.head} {...portalProps}>
      {children}
    </Portal>
  );
}
