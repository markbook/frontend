import s from './Spinner1.module.scss';
type Spinner1Props = {
  style?: JSX.HTMLAttributes<HTMLDivElement>['style'];
};
export default function Spinner1(props: Spinner1Props) {
  return (
    <div class={s.ellips} style={props.style}>
      {new Array(4).fill(0).map((_) => (
        <div />
      ))}
    </div>
  );
}
