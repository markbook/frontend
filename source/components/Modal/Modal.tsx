import s from './Modal.module.scss';
import { SVGIcon } from '@/components/Icon';
import { ComponentProps, JSX, Show } from 'solid-js';
import CloseButtonIcon from './close_button_icon.svg';
type ModalProps = {
  children?: JSX.Element;
  onCloseClick: () => void;
} & ComponentProps<typeof Show> &
  ComponentProps<'div'>;

export default function Modal(props: ModalProps) {
  return (
    <Show when={props.when}>
      <div class={s.overflow}>
        <div class={s.modal}>
          <SVGIcon icon={CloseButtonIcon} class={s.icon} clickable onClick={props.onCloseClick} />
          <div {...props} className={s.content}>
            {props.children}
          </div>
        </div>
      </div>
    </Show>
  );
}
