import s from './Footer.module.scss';
export default function Footer() {
  return (
    <footer>
      <div class={s.footerCompanyLogo} onClick={() => window.open('http://foxpro.su/')} />
    </footer>
  );
}
