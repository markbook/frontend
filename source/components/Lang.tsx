import { I18nContext, createI18nContext, useI18n } from '@solid-primitives/i18n';
import { PropsWithChildren } from 'solid-js';

export function Lang(props: PropsWithChildren) {
   const data = useData<{ isDark: true; i18n: ReturnType<typeof createI18nContext> }>(0);
   const [t, { locale }] = data.i18n;
   return (
      <I18nContext.Provider value={data.i18n}>
      </I18nContext.Provider>
   );
}
