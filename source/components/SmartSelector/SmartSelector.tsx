import s from './SmartSelector.module.scss';

export type SelectableItem = {
   title: string;
   selected: boolean;
};
type SmartSelectorProps = {
   state: SelectableItem[];
   onSelect?: (item: SelectableItem, itemIndex: number) => void;
};
export default function SmartSelector(props: SmartSelectorProps) {
   return (
      <div class={s.selector}>
         {props.state.map((item, index) => {
            const rotationValue = (360 / props.state.length) * index;
            return (
               <div
                  class={s.group}
                  style={{
                     transform: `translateX(-50%) rotateZ(${rotationValue}deg)`,
                  }}
               >
                  <label
                     classList={{
                        [s.active]: item.selected,
                     }}
                     htmlFor={item.title}
                     style={{
                        transform: `translateY(${rotationValue % 360 <= 180 ? -100 : -200}%) rotateZ(${-rotationValue}deg)`,
                        'transform-origin': rotationValue % 360 < 180 ? 'left' : 'center',
                     }}
                     innerHTML={item.title}
                  />
                  <input
                     type="radio"
                     id={item.title}
                     checked={item.selected}
                     onClick={(e) => {
                        props.onSelect && props.onSelect(item, index);
                     }}
                  />
               </div>
            );
         })}
      </div>
   );
}
