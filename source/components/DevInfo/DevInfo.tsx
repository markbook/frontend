import { Show } from 'solid-js';
import s from './DevInfo.module.scss';

type DevInfoProps = {
   data: Object;
};
export function DevInfo(props: DevInfoProps) {
   return (
      <Show when={process.env.NODE_ENV === 'development'}>
         <pre class={s.dev}>{JSON.stringify(props.data, null, 2)}</pre>{' '}
      </Show>
   );
}
