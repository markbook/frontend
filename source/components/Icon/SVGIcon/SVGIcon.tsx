import { ComponentProps } from 'solid-js';
import s from './SVGIcon.module.scss';
type SVGIconProps = {
  icon: any;
  clickable?: boolean;
} & ComponentProps<'div'>;
export default function SVGIcon({ icon, clickable, ...otherProps }: SVGIconProps) {
  return (
    <img
      src={icon}
      classList={{
        [s.clickable]: clickable,
      }}
      {...otherProps}
    />
  );
}
