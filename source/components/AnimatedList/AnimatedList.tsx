import {
  createEffect,
  createSignal,
  assignProps,
  createMemo,
  createRenderEffect,
  createComputed,
  For,
  JSX,
  mergeProps,
} from 'solid-js';

type AnimatedListProps<T, U extends JSX.Element> = {
  animationDuration?: number;
  getData: (offset: number) => T[];
  children: (item: T, index?: () => number) => U;
};

export default function AnimatedList<T, U extends JSX.Element>(props: AnimatedListProps<T, U>) {
  // < 0 = backward, > 0 - forward
  let pointer = 0;
  let rectsRefs: {
    ref: U;
    rect: DOMRect;
  }[] = [];

  props = mergeProps(props, {
    animationDuration: props.animationDuration ?? 200,
  });
  const [data, setData] = createSignal(props.getData(0));
  const [offset, setOffset] = createSignal(0);
  createComputed(() => setData(props.getData(offset())));

  const pointerOffset = createMemo(() => Math.floor(data().length / 2));

  const children = createMemo(() => props.children);

  function onElementClick(i: number) {
    pointer = i - pointerOffset();
    if (pointer === 0) return;
    setOffset(offset() + pointer);
  }
  function addRef(ref: U) {
    // adding first time after painting to ensure next animation be smooth
    if (rectsRefs.length < data().length) {
      setTimeout(() => {
        // update rect after timeout, so not using global variable insertion
        const insertion = {
          ref,
          rect: ref.getBoundingClientRect(),
        };
        rectsRefs.push(insertion);
      });
      return;
    }
    const insertion = {
      ref,
      rect: ref.getBoundingClientRect(),
    };
    if (pointer >= 0) {
      rectsRefs.push(insertion);
      if (rectsRefs.length > data().length) rectsRefs.shift();
    } else {
      // replace undefined with insertion and add undefined behind to reverse adding sequence.
      const undefIndex = rectsRefs.findIndex((x) => x === undefined);
      if (undefIndex > -1) {
        rectsRefs.splice(undefIndex, 1, insertion);
        rectsRefs.splice(undefIndex + 1, 0, undefined);
      } else {
        rectsRefs.unshift(insertion);
        rectsRefs.splice(1, 0, undefined);
      }
      if (rectsRefs.length > data().length) rectsRefs.pop();
    }
  }
  createEffect(() => {
    // let createEffect track data signal
    data();
    rectsRefs = rectsRefs.filter((x) => x != undefined);
    const dxes = rectsRefs
      .filter((x) => x.rect.left !== 0)
      .map((x) => x.rect.left - x.ref.getBoundingClientRect().left);
    if (dxes.length === 0) return;
    const newElementsCount = rectsRefs.filter((x) => x.rect.left === 0).length;
    const minDx = dxes.sort()[0];
    for (let i = 0; i < data().length; i++) {
      const mRef = rectsRefs[i];
      let dx = mRef.rect.left - mRef.ref.getBoundingClientRect().left;
      if (mRef.rect.left === 0) {
        if (pointer < 0) dx = minDx * (newElementsCount - i);
        else dx = minDx * (newElementsCount - (rectsRefs.length - i) + 1);
      }
      requestAnimationFrame(() => {
        mRef.ref.style.transform = `translateX(${dx}px)`;
        mRef.ref.style.transition = 'transform 0s';
        requestAnimationFrame(() => {
          mRef.ref.style.transform = '';
          mRef.ref.style.transition = `transform ${props.animationDuration}ms`;
          setTimeout(() => (mRef.rect = mRef.ref.getBoundingClientRect()), props.animationDuration);
        });
      });
    }
  });
  createRenderEffect(() => {
    function recalculate() {
      rectsRefs.forEach((rectRef) => {
        rectRef.rect = rectRef.ref.getBoundingClientRect();
      });
    }
    window.addEventListener('resize', () => recalculate());
    return () => {
      window.removeEventListener('resize', () => recalculate());
    };
  });
  return (
    <For each={data()}>
      {(el, i) => {
        const child = children()(el, i);
        child.addEventListener('click', () => {
          onElementClick(i());
        });
        addRef(child);
        return child;
      }}
    </For>
  );
}
