import ArrowIconRight from './assets/arrow_right.svg';
import s from './Arrow.module.scss';
type ArrowProps = {
  direction?: 'left' | 'right';
  onClick?: () => void;
};
export default function Arrow({ direction, onClick }: ArrowProps) {
  return (
    <div
      onClick={onClick}
      innerHTML={ArrowIconRight}
      classList={{ [s.arrow]: true, [s.left]: direction === 'left' }}
    />
  );
}
