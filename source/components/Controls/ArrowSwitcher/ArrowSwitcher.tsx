import Arrow from '../Arrow/Arrow';

import s from './ArrowSwitcher.module.scss';
type ArrowSwitcherProps = {
  onLeftClick?: () => void;
  onRightClick?: () => void;
};
export default function ArrowSwitcher({ onLeftClick, onRightClick }: ArrowSwitcherProps) {
  return (
    <div class={s.switcher}>
      <Arrow direction="left" onClick={onLeftClick} />
      <Arrow direction="right" onClick={onRightClick} />
    </div>
  );
}
