import { createEffect } from 'solid-js';
import './Segments.scss';

type SegmentsProps = {
   options: string[];
   selectedIndex: number;

   onSelect?: (selectIndex: number) => void;
};
export default function Segments(props: SegmentsProps) {
   let baseRef: HTMLDivElement;
   let selectorRef: HTMLDivElement;
   let optionsRef: HTMLDivElement[] = [];
   createEffect(() =>
      console.log(props.selectedIndex, optionsRef[0].getBoundingClientRect().width * props.selectedIndex)
   );
   return (
      <div class="ios13-segmented-control" ref={baseRef}>
         <span
            class="selection"
            ref={selectorRef}
            style={{
               transform: `translateX(${Math.floor(
                  optionsRef[0].getBoundingClientRect().width * props.selectedIndex
               )}px)`,
            }}
         />
         {props.options.map((x, i) => (
            <div class="option">
               <input
                  type="radio"
                  id={x}
                  value={x}
                  checked
                  ref={(r) => optionsRef.push(r)}
                  onClick={() => props.onSelect && props.onSelect(i)}
               />
               <label for={x}>
                  <span>{x}</span>
               </label>
            </div>
         ))}
      </div>
   );
}
