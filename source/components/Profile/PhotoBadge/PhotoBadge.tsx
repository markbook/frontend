import ProfilePicture from './assets/ProfilePicture.png';

import s from './PhotoBadge.module.scss';

type PhotoBadgeProps = {
  photoSrc?: string;
  fullName: string;
  email?: string;
};
export default function PhotoBadge(props: PhotoBadgeProps) {
  return (
    <div class={s.container}>
      <img src={ProfilePicture} class={s.photo} />
      <div class={s.name}>{props.fullName}</div>
      <div class={s.email}>{props.email}</div>
    </div>
  );
}
