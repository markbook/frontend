import { CourseClient, AuthClient, UserClient, LoginResponse } from './API';
import { IConfig } from './APIBase';

let jwt: string | null = null;
const key = 'abrakadabra';
const host = 'http://localhost:5000';

export function getJWT()
{
   if (jwt != null) return 'Bearer ' + jwt;
   const rawData = localStorage.getItem(key);
   if (rawData != null)
   {
      jwt = (JSON.parse(rawData) as LoginResponse).accessToken!;
   }
   if (jwt == null) return null;
   console.log('loaded jwt', jwt);
   return 'Bearer ' + jwt;
}
export function setAuthData(data: LoginResponse)
{
   localStorage.setItem(key, JSON.stringify(data));
}
export function isJWTAvaiable()
{
   var rawData = localStorage.getItem(key);
   if (!rawData) return false;
   const storedData = JSON.parse(rawData) as LoginResponse;
   return storedData?.accessToken && storedData?.expiresIn < Date.now();
}
const configuration: IConfig = {
   getAuthorization: () => getJWT() ?? '',
};

export const courseClient = new CourseClient(configuration, host);
export const authClient = new AuthClient(configuration, host);
export const userClient = new UserClient(configuration, host);
