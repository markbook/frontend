import { JSX } from 'solid-js/jsx-runtime';
import { LoginResponse, ScheduleObject, User } from '@/api/API';
import { setAuthData, isJWTAvaiable, userClient } from '@/api/APIProvider';
import { createContext, useContext, createEffect } from 'solid-js';
import { createStore } from 'solid-js/store';

type AppStore = [
   {
      schedule?: ScheduleObject[];
      isAuthorized: boolean;
      user?: User;
      networkErrorModal: {
         showing: boolean;
         errorText: string;
      };
   },
   {
      authorize: (data: LoginResponse) => void;
      showErrorModal: (erorrText: string) => void;
      hideErrorModal: () => void;
   }
];
const initialState: AppStore = [
   {
      schedule: [],
      isAuthorized: false,
      networkErrorModal: {
         showing: false,
         errorText: '',
      },
   },
   {
      authorize: () => {},
      showErrorModal: () => {},
      hideErrorModal: () => {},
   },
];
const AppContext = createContext(initialState);

type AppProviderProps = {
   children: JSX.Element[] | JSX.Element;
};
export function AppProvider(props: AppProviderProps) {
   const [state, setState] = createStore(initialState[0]);
   const store: AppStore = [
      state,
      {
         authorize(data) {
            setAuthData(data);
            setState('isAuthorized', true);
         },
         showErrorModal(errorText) {
            setState('networkErrorModal', {
               showing: true,
               errorText,
            });
         },
         hideErrorModal() {
            setState('networkErrorModal', {
               showing: false,
               errorText: '',
            });
         },
      },
   ];
   createEffect(async () => {
      setState('isAuthorized', true);
   });

   return <AppContext.Provider value={store}>{props.children}</AppContext.Provider>;
}

export function useAppStore() {
   return useContext(AppContext);
}
