import { GeocoderResponse } from './types';

const apiKey = 'e50d471e-c1c1-425b-956d-93a827fd5892';

type GeocoderQueryParams = {
  geocode: string;
  results?: number;
  skip?: number;
  lang?: 'ru_RU' | 'uk_UA' | 'be_BY' | 'en_US';
};
export function query({ geocode, results = 5 }: GeocoderQueryParams) {
  const url = encodeURI(
    `https://geocode-maps.yandex.ru/1.x/?apikey=${apiKey}&geocode=${geocode}&format=json&results=${results}`
  );
  return fetch(url).then((x) => x.json()) as Promise<GeocoderResponse>;
}
