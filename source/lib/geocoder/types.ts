export interface GeocoderResponse {
  GeoObjectCollection: GeoObjectCollection;
}

export interface GeoObjectCollection {
  metaDataProperty: GeoObjectCollectionMetaDataProperty;
  featureMember: FeatureMember[];
}

export interface FeatureMember {
  GeoObject: GeoObject;
}

export interface GeoObject {
  metaDataProperty: GeoObjectMetaDataProperty;
  name: string;
  description: string;
  boundedBy: BoundedBy;
  Point: Point;
}

export interface Point {
  pos: string;
}

export interface BoundedBy {
  Envelope: Envelope;
}

export interface Envelope {
  lowerCorner: string;
  upperCorner: string;
}

export interface GeoObjectMetaDataProperty {
  GeocoderMetaData: GeocoderMetaData;
}

export interface GeocoderMetaData {
  precision: Precision;
  text: string;
  kind: Kind;
  Address: Address;
  AddressDetails: AddressDetails;
}

export interface Address {
  country_code: CountryCode;
  formatted: string;
  postal_code?: string;
  Components: Component[];
}

export interface Component {
  kind: Kind;
  name: string;
}

export enum Kind {
  Area = 'area',
  Country = 'country',
  House = 'house',
  Locality = 'locality',
  Province = 'province',
  Street = 'street',
}

export enum CountryCode {
  Kg = 'KG',
  Ru = 'RU',
  Ua = 'UA',
}

export interface AddressDetails {
  Country: Country;
}

export interface Country {
  AddressLine: string;
  CountryNameCode: CountryCode;
  CountryName: CountryName;
  AdministrativeArea: AdministrativeArea;
}

export interface AdministrativeArea {
  AdministrativeAreaName: string;
  Locality?: Locality;
  SubAdministrativeArea?: SubAdministrativeArea;
}

export interface Locality {
  LocalityName: string;
  Thoroughfare: Thoroughfare;
}

export interface Thoroughfare {
  ThoroughfareName: string;
  Premise: Premise;
}

export interface Premise {
  PremiseNumber: string;
  PostalCode?: PostalCode;
}

export interface PostalCode {
  PostalCodeNumber: string;
}

export interface SubAdministrativeArea {
  SubAdministrativeAreaName: string;
  Locality: Locality;
}

export enum CountryName {
  Киргизия = 'Киргизия',
  Россия = 'Россия',
  Украина = 'Украина',
}

export enum Precision {
  Exact = 'exact',
}

export interface GeoObjectCollectionMetaDataProperty {
  GeocoderResponseMetaData: GeocoderResponseMetaData;
}

export interface GeocoderResponseMetaData {
  request: string;
  results: string;
  found: string;
}
