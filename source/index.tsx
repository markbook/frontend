import { Router } from 'solid-app-router';
import { createApp } from 'solid-utils';

import App from './views/App';
// if (navigator.userAgent.indexOf('Firefox') > -1) {
//   navigator.serviceWorker.register('serviceWorker.js', {
//     type: 'module',
//   });
// }
createApp(App).mount('#app');
