import { useAppStore } from '@/stores/AppStore';
import { createRenderEffect } from 'solid-js';
import Modal from '@/components/Modal';

export default function GlobalErrorWrapper() {
   const [store, { showErrorModal, hideErrorModal }] = useAppStore();
   createRenderEffect(() => {
      window.addEventListener('unhandledrejection', (ev) => {
         console.log(ev);
         showErrorModal(ev.reason.response || ev.reason);
      });
   });
   return (
      <>
         <Modal
            when={store.networkErrorModal.showing}
            onCloseClick={() => {
               hideErrorModal();
            }}
         >
            Произошла ошибка: {store.networkErrorModal.errorText}
         </Modal>
      </>
   );
}
