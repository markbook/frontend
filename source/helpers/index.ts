import { format, formatDuration } from 'date-fns';
import ru from 'date-fns/locale/ru';
export const dateFnsFormat: typeof format = (date, f, options) =>
  format(date, f, { ...options, locale: ru });
export const dateFnsFormatDuration: typeof formatDuration = (date, options) =>
  formatDuration(date, { ...options, locale: ru });
